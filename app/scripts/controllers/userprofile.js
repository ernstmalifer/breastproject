'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:UserProfileCtrl
 * @description
 * # UserProfileCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('UserProfileCtrl', function (ENV, $scope, User, $location, userService, requestService, _, growl) {
    $scope.user = User;

    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
    });

    $scope.save = function() {
      growl.info("Saving Information", {ttl: 1000, disableCountDown: true});

      var user = {
        u_fname: $scope.user.u_fname,
        u_lname: $scope.user.u_lname,
        department: $scope.user.department,
        institution: $scope.user.institution,
        u_email: $scope.user.u_email
      }

      userService.updateUser(User._id, user).then(function(resp){
        growl.success("Information Saved", {ttl: 1000, disableCountDown: true});
      })
    }
    $scope.cancel = function() {

    }

    $scope.onUpload = function(data){
        growl.info("Uploading Profile Picture", {ttl: 1000, disableCountDown: true});
    }

    $scope.onComplete = function(resp){
        growl.success("Profile Picture Uploaded", {ttl: 1000, disableCountDown: true});

        userService.islogin().then(function(resp){
            if (resp.data == '0') {
              $window.location.href='#/404';
            }
            else {

              // Get User Data
              User.loggedIn = true;
              User._id = resp.data._id
              User.u_email = resp.data.email

              // Get User Profile
              userService.getUser(User._id).then(function(resp){
                User.u_fname= resp.data.result.u_fname
                User.u_mi = resp.data.result.u_mi
                User.u_lname= resp.data.result.u_lname
                User.department= resp.data.result.department
                User.institution= resp.data.result.institution
                User.postaladdress= resp.data.result.postal_address
                User.city_suburb= resp.data.result.city_suburb
                User.phone= resp.data.result.phone
                User.fax= resp.data.result.fax
                User.mobile= resp.data.result.mobile
                User.email= resp.data.result.email
                User.avatar= resp.data.result.u_image
              });

            }
        });
    }

  });
