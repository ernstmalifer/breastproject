'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('LoginCtrl', function ($scope, $stateParams, $location, $modal, User, userService, requestService, inquiriesService, $http, $window, growl) {

    //provide initializer if user is logged in
    User.loggedIn = false;
    $scope.rightSideBar = false;
    $scope.inquiry = {};

    $scope.$on('$viewContentLoaded', function() {

      $scope.disablelogin = false;

      userService.islogin().then(function(resp){
        if (resp.data !== '0') {
          User.loggedIn = true;
          $scope.rightSideBar = false;
        } else {
          User.loggedIn = false;
          $scope.rightSideBar = true;
        }
      });
    });

    if($stateParams.login){
      $scope.loginFalse = true;
    }

    $scope.login = function(){

      $scope.disablelogin = true;

      userService.login($scope.username, $scope.password).then(
        function(resp) {

          if(resp.data._id) {
            User._id = resp.data._id
            userService.islogin().then(function(user) {
            });
            if(resp.data.roleID.RoleDesc == 'Researcher') {
              //if application of access is still pending
              requestService.getUnapprovedRequestByUser(User._id).then(function(resp){
                if (resp.data.result != null){
                  if(resp.data.result.approve_tag == 'NO'){
                    if(resp.data.result.redirectlink) {
                      $window.location.href= resp.data.result.redirectlink;
                    } else {
                      $window.location.href='#/registration/projecttitle';
                    }
                    // $location.path( '/registration/projecttitle?true' );
                  } else {
                  }
                } else {
                  $window.location.href='#/search';
                  // $location.path( '/search?true' );
                }
              });

            } else if(resp.data.roleID.RoleDesc == 'Administrator') {
              $window.location.href='#/admin';
            }
          } else {
            $scope.disablelogin = false;
            $window.location.href='#/forgotpassword?login=false';
          }
        });

      $scope.disablelogin = false;
    };

    var myOtherModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/contactus.tpl.html',
      show: false
    });

    $scope.showModal = function() {
      myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.sendInquiry = function(){

      if(validate()) {
        growl.info("Sending Inquiry", {ttl: 1000, disableCountDown: true});
        myOtherModal.$promise.then(myOtherModal.hide);
        inquiriesService.saveInquiry($scope.inquiry).then(function(data){
          growl.success("Inquiry Sent", {ttl: 1000, disableCountDown: true});
        })
      } else {
        growl.error("Please complete all the required fields.", {ttl: 1000, disableCountDown: true});
      }

    }

    function validate(){
      
      $scope.validateFirstName()
      $scope.validateLastName()
      $scope.validateEMail()
      $scope.validatePhone()
      $scope.validateInstitution()
      $scope.validateSubject()
      $scope.validateMessType()
      $scope.validateMessage()

      if( !$scope.inquiryufname_error &&
        !$scope.inquiryulname_error &&
        !$scope.inquiryuemail_error &&
        !$scope.inquiryphone_error &&
        !$scope.inquiryinstitution_error &&
        !$scope.inquirysubject_error &&
        !$scope.inquirymesstype_error &&
        !$scope.inquirymessage_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateFirstName = function(){

      $scope.inquiryufname_error = false

      if(validator.isNull($scope.inquiry.u_fname)){
        $scope.inquiryufname_error = true;
        growl.error("Please enter your First Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.inquiry.u_fname)){
          $scope.inquiryufname_error = true;
          growl.error("First Name only contains alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateLastName = function(){
      
      $scope.inquiryulname_error = false

      if(validator.isNull($scope.inquiry.u_lname)){
        $scope.inquiryulname_error = true;
        growl.error("Please enter your Last Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.inquiry.u_lname)){
          $scope.inquiryulname_error = true;
          growl.error("Last Name only contains alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateEMail = function(){
      
      $scope.inquiryuemail_error = false

      if(validator.isNull($scope.inquiry.u_email)){
        $scope.inquiryuemail_error = true
        growl.error("Please enter E-Mail Address", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isEmail($scope.inquiry.u_email)){
          $scope.inquiryuemail_error = true
          growl.error("Please enter a valid E-Mail Address", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validatePhone = function(){
      
      $scope.inquiryphone_error = false

      if(validator.isNull($scope.inquiry.phone)){
        $scope.inquiryphone_error = true;
        growl.error("Please enter your Contact Number", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateInstitution = function(){
      
      $scope.inquiryinstitution_error = false

      if(validator.isNull($scope.inquiry.institution)){
        $scope.inquiryinstitution_error = true;
        growl.error("Please enter your Institution", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateSubject = function(){
      
      $scope.inquirysubject_error = false

      if(validator.isNull($scope.inquiry.subject)){
        $scope.inquirysubject_error = true;
        growl.error("Please enter the Subject", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateMessType = function(){

      $scope.inquirymesstype_error = false
      
      if(validator.isNull($scope.inquiry.messtype)){
        $scope.inquirymesstype_error = true;
        growl.error("Please enter the Message Type", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateMessage = function(){
      
      $scope.inquirymessage_error = false

      if(validator.isNull($scope.inquiry.message)){
        $scope.inquirymessage_error = true;
        growl.error("Please enter your Message", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.recoverPassword = function(){

      $scope.recoverPasswordEmail_error = false

      if(validator.isNull($scope.recoverPasswordEmail)){
        $scope.recoverPasswordEmail_error = true
        growl.error("Please enter E-Mail Address", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isEmail($scope.recoverPasswordEmail)){
          $scope.recoverPasswordEmail_error = true
          growl.error("Please enter a valid E-Mail Address", {ttl: 1000, disableCountDown: true});
        } else {
          userService.recoverPassword($scope.recoverPasswordEmail).then(function(resp){
            if(resp.status == 200){
              growl.success("A link is sent to your your email address to update your password", {ttl: 1000, disableCountDown: true});
            } else {
              $window.location.href='#/';
              growl.success("EMail address doesn't exists", {ttl: 1000, disableCountDown: true});
            }
          })
        }
      }
      
    }

  });
