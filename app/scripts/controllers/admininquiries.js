'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminInquiriesCtrl', function ($scope, User, $modal, inquiriesService, $window, growl) {
    User.loggedIn = true;
  	$scope.user = User;

    $scope.$on('$viewContentLoaded', function() {

      growl.info("Loading Inquiries", {ttl: 1000, disableCountDown: true});

      $scope.selectAllI = false;

      loadData()
    });

    // Inquiries

    var rejectInquiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/rejectInquiryModal.html',
      show: false
    });

    $scope.showRejectInquiryModal = function(inquiryid){

      $scope.messagereject = ""
      $scope.inquirytoremove = inquiryid;
      rejectInquiryModal.$promise.then(rejectInquiryModal.show());

      if(inquiryid == 'selected'){
        $scope.selectedInquiries = $scope.getAllSelectedInquiries();
      } else {
        $scope.selectedInquiries = $scope.getInquiry(inquiryid)
      }
    }


    $scope.rejectInquiry = function(message){
      growl.info("Rejecting Inquiry", {ttl: 1000, disableCountDown: true});

      if($scope.inquirytoremove == 'selected'){ //bulk

        var selectedInquiries = $scope.selectedInquiries;

        _.each(selectedInquiries, function(inquiry){
          inquiry.r_message = message;
          inquiriesService.rejectInquiry(inquiry).then(function(resp){
          })
        });

        loadData();
        growl.success("Inquiries Rejected", {ttl: 1000, disableCountDown: true});

      } else {
        $scope.selectedInquiries[0].r_message = message;
        inquiriesService.rejectInquiry($scope.selectedInquiries).then(function(resp){

          loadData();
        })
      }
    }

    $scope.getInquiry = function(inquiryid){
      return _.filter($scope.inquiries, function(inquiry){ return inquiry._id == inquiryid })
    }

    $scope.hasSelectedI = function(){
      var selected = $scope.countSelectedInquiries()
      if(selected.selected > 0){ return true; }
      else { return false; }
    }

    $scope.selectAllInquiries = function(){
      $scope.inquiries = _.each($scope.inquiries, function(inquiry){
        inquiry.selected = $scope.selectAllI;
      })
    }

    $scope.countSelectedInquiries = function(){

      var selected = _.countBy($scope.inquiries, function(inquiry) {
        return inquiry.selected == true ? 'selected': 'not-selected';
      });

      return selected;
    }

    $scope.getAllSelectedInquiries = function(){
      return _.filter($scope.inquiries, function(inquiry){
        return inquiry.selected == true;
      })
    }

    var approveInquiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/approveInquiryModal.html',
      show: false
    });

    $scope.showApproveInquiryModal = function(inquiryid){

      $scope.messageapprove = ""
      $scope.inquirytoapprove = inquiryid;
      approveInquiryModal.$promise.then(approveInquiryModal.show());

      if(inquiryid == 'selected'){
        $scope.selectedInquiries = $scope.getAllSelectedInquiries();
      } else {
        $scope.selectedInquiries = $scope.getInquiry(inquiryid)
      }

    }

    $scope.approveInquiry = function(message){

      growl.info("Approving Inquiry & Creating User Account", {ttl: 1000, disableCountDown: true});

      var selectedInquiries = $scope.selectedInquiries;

      if($scope.inquirytoapprove == 'selected'){ //bulk

        _.each(selectedInquiries, function(inquiry){
          inquiry.r_message = message;
          inquiriesService.approveInquiry(inquiry._id).then(function(resp){
            if(inquiry._id == selectedInquiries[selectedInquiries.length - 1]._id){
              loadData();
              growl.success("Inquiries Rejected", {ttl: 1000, disableCountDown: true});
            }
          })
        });

      } else {
        inquiriesService.approveInquiry(selectedInquiries[0]._id).then(function(resp){
          inquiriesService.getInquiries().then(function(resp){
            if(resp.data.result != null) {
              $scope.inquiries = resp.data.result;
              growl.success("Inquiry Approved & User is created account", {ttl: 2000, disableCountDown: true});
              loadData();
            }
          })
        })
      }

    }

    function loadData(){
      inquiriesService.getInquiries().then(function(resp){
        if(resp.data.result != null) {
          $scope.inquiries = resp.data.result;
          $scope.inquiries = _.each($scope.inquiries, function(inquiry){
            inquiry.selected = false;
          })
          growl.success("Inquiries Loaded", {ttl: 1000, disableCountDown: true});
        }
      })


      $scope.selectAllI = false;
    }

    $scope.removeInquiry = function(inquiryid){
      
      var deletethis = confirm("Are you sure you want to delete this inquiry?");
      if (deletethis == true) {
          inquiriesService.removeInquiry(inquiryid).then(function(resp){
            
          })
          location.reload();
          $window.location.href='#/admin/inquiries';
      } else {
      }
    }


  });
