'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminUsersCtrl
 * @description
 * # AdminUsersCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminUsersCtrl', function (ENV, $scope, User, $modal, userService, growl, _) {
    User.loggedIn = true;
  	$scope.user = User;
    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
      
      $scope.selectAll = false;
      loadData();

      userService.getRoles().then(function(resp){
        if(resp.data.result != null) {
          $scope.roles = resp.data.result
          $scope.rolesSelect = [];

          _.each($scope.roles, function(role){
            var roleitem = { label: role.RoleDesc, id: role._id}
            $scope.rolesSelect.push(roleitem)
          })
        }
      })

    });

    $scope.$on('$viewContentLoaded', function() {
    });

    $scope.selectAllUsers = function(){
      $scope.users = _.each($scope.users, function(user){
        user.selected = $scope.selectAll;
      })
    }

    $scope.getUser = function(userid){
      return _.filter($scope.users, function(user){ return user.userID._id == userid })
    }

    var deleteUserModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/deleteUser.html',
      show: false
    });

    $scope.showDeleteUserModal = function(userid){
      $scope.useridtodelete = userid;
      deleteUserModal.$promise.then(deleteUserModal.show());

      if($scope.useridtodelete == 'selected'){
        $scope.selectedUsers = $scope.getAllSelectedUsers();
      } else {
        $scope.selectedUsers = $scope.getUser($scope.useridtodelete)
      }
    }

    $scope.deleteUsers = function(){

      if($scope.useridtodelete == 'selected'){ //bulk

        var selectedUsers = $scope.getAllSelectedUsers();

        _.each(selectedUsers, function(user){
          userService.deleteUser(user.userID._id).then(function(resp){
          })
        });

        loadData();
        growl.success("Users deleted", {ttl: 1000, disableCountDown: true});

      } else {
        userService.deleteUser($scope.useridtodelete).then(function(resp){
          loadData();
          growl.success("User deleted", {ttl: 1000, disableCountDown: true});
        })
      }
    }

    function loadData(){
      growl.info("Loading Users", {ttl: 1000, disableCountDown: true});

      userService.getAllUsers().then(function(resp){
        if(resp.data.result != null) {
          $scope.users = resp.data.result
          growl.success("Users Loaded", {ttl: 1000, disableCountDown: true});
        }
      })

    }

    $scope.getAllSelectedUsers = function(){
      return _.filter($scope.users, function(user){
        return user.selected == true;
      })
    }

    $scope.hasSelected = function(){
      var selected = $scope.countSelectedUsers()
      if(selected.selected > 0){ return true; }
      else { return false; }
    }

    $scope.countSelectedUsers = function(){

      var selected = _.countBy($scope.users, function(user) {
        return user.selected == true ? 'selected': 'not-selected';
      });

      return selected;
    }

    var addUser = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/newUser.html',
      show: false
    });

    $scope.showAddUserModal = function() {
      $scope.newuser = {};
      addUser.$promise.then(addUser.show);
    };

    $scope.createUser = function(){
      if(validate()){
        growl.info("Creating User", {ttl: 1000, disableCountDown: true});
        userService.createUser($scope.newuser).then(function(resp){
          growl.success("User created", {ttl: 1000, disableCountDown: true});
          addUser.$promise.then(addUser.hide);
          loadData();
        })
      }
    }

    function validate(){
      
      $scope.validateFirstName()
      $scope.validateLastName()
      $scope.validateEMail()
      $scope.validateDepartment()
      $scope.validateInstitution()
      $scope.validateRole()

      if( !$scope.newuserufname_error &&
        !$scope.newuserulname_error &&
        !$scope.newuseruemail_error &&
        !$scope.newuserdepartment_error &&
        !$scope.newuserinstitution_error &&
        !$scope.newuserrole_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateFirstName = function(){

      $scope.newuserufname_error = false

      if(validator.isNull($scope.newuser.u_fname)){
        $scope.newuserufname_error = true;
        growl.error("Please enter your First Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.newuser.u_fname)){
          $scope.newuserufname_error = true;
          growl.error("First Name only contains alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateLastName = function(){
      
      $scope.newuserulname_error = false

      if(validator.isNull($scope.newuser.u_lname)){
        $scope.newuserulname_error = true;
        growl.error("Please enter your Last Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.newuser.u_lname)){
          $scope.newuserulname_error = true;
          growl.error("Last Name only contains alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateEMail = function(){
      
      $scope.newuseruemail_error = false

      if(validator.isNull($scope.newuser.u_email)){
        $scope.newuseruemail_error = true
        growl.error("Please enter E-Mail Address", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isEmail($scope.newuser.u_email)){
          $scope.newuseruemail_error = true
          growl.error("Please enter a valid E-Mail Address", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateInstitution = function(){
      
      $scope.newuserinstitution_error = false

      if(validator.isNull($scope.newuser.institution)){
        $scope.newuserinstitution_error = true;
        growl.error("Please enter your Institution", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateDepartment = function(){
      
      $scope.newuserdepartment_error = false

      if(validator.isNull($scope.newuser.department)){
        $scope.newuserdepartment_error = true;
        growl.error("Please enter your Department", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateRole = function(){

      $scope.newuserrole_error = false
      
      if(validator.isNull($scope.newuser.roleID)){
        $scope.newuserrole_error = true;
        growl.error("Please select a Role", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

  });
