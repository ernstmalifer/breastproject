'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration6Ctrl', function (ENV, $rootScope, $scope, User, $location, requestService, _, growl, $timeout, $modal) {

    User.loggedIn = true;
    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
    });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {
      loadData()
    });

    $scope.popover = {};

    $scope.back = function(){
      $location.path( '/registration/certificationbyapplicant' );
    };

    var submitModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/submitregistration.html',
      show: false
    });

    $scope.showModal = function() {
      submitModal.$promise.then(submitModal.show);
    };

    $scope.save = function(){

      $scope.saving = true;

      growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

      requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
        $scope.saving = false;
        growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
      })
    };

    var input;
    var inputmodel;

    $scope.showUpload = function(element){      
      $timeout(function() {
        input = angular.element(element + ' input')
        input.trigger('click');
        //get id by string
        inputmodel = element.substring(7);
        document.body.onfocus = function () { setTimeout(checkOnCancel, 100); };
      }, 50);
    }


    function checkOnCancel()
    {
        if(input[0].value.length == 0) {
          $scope.request[inputmodel]['yes'] = false;
          // alert('You clicked cancel - ' + "FileName:" + input[0].value + "; Length: " + input[0].value.length)
        }
        else {
          // alert('You selected a file - ' + "FileName:" + input[0].value + "; Length: " + input[0].value.length);
        }

        document.body.onfocus = null;
    }

    $scope.next = function(){

      growl.info("Submitting Request", {ttl: 1000, disableCountDown: true});

      requestService.submitRequest(User.requestid).then(function(resp){
        growl.success("The request has been submitted to the administrators", {ttl: 1000, disableCountDown: true});
      })
      $scope.showModal()
    };

    $scope.gotoHome = function(){
      $location.path( '/' );
    }

    $scope.onUpload = function(data, document){
      growl.info("Uploading " + document, {ttl: 1000, disableCountDown: true});
    }

    $scope.onComplete = function(resp){
      growl.success("Document Uploaded", {ttl: 1000, disableCountDown: true});
      loadData();
    }

    $scope.setEXT = function(filename){
      return filename.split(".")[1]
    }

    function loadData(){
      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];
          $scope.request.redirectlink = window.location.hash;
          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

          if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
              },
              onopen: function() {
              }
            });
          }
        }
      })
    }

  });
