'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration2Ctrl', function ($rootScope, $scope, User, $location, requestService, _, growl) {

  	User.loggedIn = true;

    $scope.otherResearchersToAdd = {
      orcp_fullname: '',
      orcp_department: '',
      orcp_institution: ''
    }

    $scope.fromDate = new Date();

  	$scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
	  });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {
      
      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];
          $scope.request.redirectlink = window.location.hash;
          
          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

          if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
              },
              onopen: function() {
              }
            });
          }

          CKEDITOR.replace( 'summary' );
          CKEDITOR.replace( 'aim' );
          CKEDITOR.replace( 'methodology' );
          CKEDITOR.replace( 'statisticaljustification' );

          CKEDITOR.instances.summary.on('blur', function(event){
            $scope.validateSummary();
          })
          CKEDITOR.instances.aim.on('blur', function(event){
            $scope.validateAim();
          })
          CKEDITOR.instances.methodology.on('blur', function(event){
            $scope.validateMethodology();
          })
          CKEDITOR.instances.statisticaljustification.on('blur', function(event){
            $scope.validateStatisticalJustification();
          })

          if($scope.request.orcp.length < 3) {
            console.log('yow', $scope.request.orcp.length - 3)
            for( var i = ($scope.request.orcp.length - 3) ; i < 0; ++i) {
              $scope.request.orcp.push({orcp_fullname: ''});
            }
          }

          // console.log($scope.request.startdate)
          if($scope.request.startdate == undefined){
            $scope.request.startdate = $scope.fromDate;
          }

          if($scope.request.enddate == undefined){
            $scope.request.enddate = moment().add(30, 'days').calendar();
          }
        }
      })
    });

    $scope.addOtherResearchersCollaboratingOnProject = function(){
      $scope.request.orcp.push({orcp_fullname: ''});
      // if($scope.otherResearchersToAdd.orcp_fullname != '' && $scope.otherResearchersToAdd.orcp_department != '' && $scope.otherResearchersToAdd.orcp_institution != ''){
      //   $scope.request.orcp.push($scope.otherResearchersToAdd);
      //   clearOtherResearchersToAdd();
      // } else {
      //   growl.error("Please enter a fullname, department and institution before adding.", {ttl: 1000, disableCountDown: true});
      // }
    }

    $scope.removeOtherResearchersCollaboratingOnProject = function(otherResearcher) {
      // console.log(otherResearcher)
      $scope.request.orcp.splice(otherResearcher, 1) 
    }

    function clearOtherResearchersToAdd(){
      $scope.otherResearchersToAdd = {
        orcp_fullname: '',
        orcp_department: '',
        orcp_institution: ''
      }
    }

  	$scope.back = function(){
  		$location.path( '/registration/projecttitle' );
  	};

  	$scope.save = function(){

      $scope.saving = true;

      if(validate()){
        $scope.request.summaryofresearch = CKEDITOR.instances.summary.getData()
        $scope.request.researchplaninfo = CKEDITOR.instances.aim.getData()
        $scope.request.projectmethodology = CKEDITOR.instances.methodology.getData()
        $scope.request.statisticaljustification = CKEDITOR.instances.statisticaljustification.getData()

        growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

        requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
          $scope.saving = false;
          growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
        })
      } else {
        $scope.saving = false;
        growl.error("Please enter required fields before saving.", {ttl: 1000, disableCountDown: true});
      }

  	};

  	$scope.next = function(){
      if(validate()){
        $scope.save();
  		  $location.path( '/registration/funding' );
      } else {
        growl.error("Please enter required fields and save before continue.", {ttl: 1000, disableCountDown: true});
      }
  	};

    function validate(){

      $scope.validateSummary();
      $scope.validateAim();
      $scope.validateMethodology();
      // $scope.validateDataReqNumber();
      // $scope.validateTypeOfCase();
      // $scope.validateNumberOfCase();
      // $scope.validateEquipment();
      $scope.validateStatisticalJustification();
      $scope.validateStartDate();
      $scope.validateEndDate();

      if( !$scope.summary_error &&
        !$scope.aim_error &&
        !$scope.methodology_error &&
        // !$scope.datareqnumber_error &&
        // !$scope.typeofcase_error &&
        // !$scope.numberofcaseerror &&
        // !$scope.equipment_error &&
        !$scope.statisticaljustification_error &&
        !$scope.startdate_error &&
        !$scope.enddate_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateSummary = function(){

      $scope.summary_error = false

      if(validator.isNull(CKEDITOR.instances.summary.getData())){
        $scope.summary_error = true;
        growl.error("Please enter Summary of Project", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateAim = function(){

      $scope.aim_error = false

      if(validator.isNull(CKEDITOR.instances.aim.getData())){
        $scope.aim_error = true;
        growl.error("Please enter Aim of Research", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateMethodology = function(){

      $scope.methodology_error = false

      if(validator.isNull(CKEDITOR.instances.methodology.getData())){
        $scope.methodology_error = true;
        growl.error("Please enter Methodology of Research", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    // $scope.validateDataReqNumber = function(){

    //   $scope.datareqnumber_error = false

    //   if(validator.isNull($scope.request.datareqnumber)){
    //     $scope.datareqnumber_error = true;
    //     growl.error("Please enter Number of Readers whom data are requested", {ttl: 1000, disableCountDown: true});
    //   } else {
    //     if(!validator.isInt($scope.request.datareqnumber, { min: 0})){
    //       $scope.datareqnumber_error = true;
    //       growl.error("Number of Readers whom data are requested should be a number", {ttl: 1000, disableCountDown: true});
    //     }
    //   }
    // }

    // $scope.validateTypeOfCase = function(){

    //   $scope.typeofcase_error = false

    //   if(validator.isNull($scope.request.typeofcase)){
    //     $scope.typeofcase_error = true;
    //     growl.error("Please enter Type of Case", {ttl: 1000, disableCountDown: true});
    //   } else {
    //   }
    // }

    // $scope.validateNumberOfCase = function(){

    //   $scope.numberofcase_error = false

    //   if(validator.isNull($scope.request.numberofcase)){
    //     $scope.numberofcase_error = true;
    //     growl.error("Please enter Number of Cases", {ttl: 1000, disableCountDown: true});
    //   } else {
    //     if(!validator.isInt($scope.request.numberofcase, { min: 0})){
    //       $scope.numberofcase_error = true;
    //       growl.error("Number of Cases should be a number", {ttl: 1000, disableCountDown: true});
    //     }
    //   }
    // }

    // $scope.validateEquipment = function(){

    //   $scope.equipment_error = false

    //   if(validator.isNull($scope.request.equipment)){
    //     $scope.equipment_error = true;
    //     growl.error("Please enter Equipment", {ttl: 1000, disableCountDown: true});
    //   } else {
    //   }
    // }

    $scope.validateStatisticalJustification = function(){

      $scope.statisticaljustification_error = false

      if(validator.isNull(CKEDITOR.instances.statisticaljustification.getData())){
        $scope.statisticaljustification_error = true;
        growl.error("Please enter Statistical Justification", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateStartDate = function(){

      $scope.startdate_error = false

      if(validator.isNull($scope.request.startdate)){
        $scope.startdate_error = true;
        growl.error("Please enter Start Date", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateEndDate = function(){

      $scope.enddate_error = false

      if(validator.isNull($scope.request.enddate)){
        $scope.enddate_error = true;
        growl.error("Please enter End Date", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

  });
