'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('UserCtrl', function (ENV, $scope, User, $location, userService, requestService, $window, growl) {
  	$scope.user = User;

    $scope.serverIP = ENV.apiEndpoint;
    
    $scope.$on('$viewContentLoaded', function() {
    });

    $scope.logout = function() {
      userService.logout().then(function(resp){
        location.hash = '#/';
        location.reload();
      })
    }

    $scope.clickUser = function(){
      requestService.getUnapprovedRequestByUser($scope.user._id).then(function(resp){
        if (resp.data.result != null){
          if(resp.data.result.approve_tag == 'NO'){
            if(resp.data.result.redirectlink) {
              growl.error("Your Application for Access isn't approved", {ttl: 1000, disableCountDown: true});
              $window.location.href= resp.data.result.redirectlink;
            } else {
              $window.location.href='#/registration/projecttitle';
            }
            // $location.path( '/registration/projecttitle?true' );
          } else {
          }
        } else {
          $window.location.href='#/search';
          // $location.path( '/search?true' );
        }
      });
    }

  });

angular.module('breastApp')
  .controller('HeaderCtrl', function (ENV, $rootScope, $scope, User, $location, userService, requestService) {
    $scope.user = User;
    $scope.href = '#/';

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {
      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          if(resp.data.result[0].approve_tag == "NO"){
            if(resp.data.result[0].redirectlink) {
              $scope.href = resp.data.result[0].redirectlink;
            }
          }

        }
      })       
    })


  });