'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
