'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:VerifyCtrl
 * @description
 * # VerifyCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('VerifyCtrl', function ($scope, $stateParams, $location, $modal, User, userService, requestService, inquiriesService, $http, $window, growl) {

    //provide initializer if user is logged in
    User.loggedIn = false;
    $scope.rightSideBar = true;
    $scope.inquiry = {};

    $scope.$on('$viewContentLoaded', function() {

      $scope.disablelogin = false;

      userService.islogin().then(function(resp){
        if (resp.data !== '0') {
          User.loggedIn = true;
          $scope.rightSideBar = false;
        } else {
          User.loggedIn = false;
          $scope.rightSideBar = true;
        }
      });

      userService.getUser($stateParams.id).then(function(resp){
        $scope.user = resp.data.result;
        if(resp.data.result.userID.u_isverified == 0) {

        } else {
          $window.location.href='#/404';
        }
      })
    });

    $scope.verify = function(){
      userService.verify($stateParams.id, $scope.password, $scope.confirm).then(function(resp){
        if(resp.data.statusCode === 200){
          login()
        }else{
          alert(response[0].msg);
        }
      })

    }

    function login(){

      userService.login($scope.user.userID.u_email, $scope.password).then(function(resp) {
        if(resp.data._id) {
          User._id = resp.data._id
          userService.islogin().then(function(user) {
          });
          if(resp.data.roleID.RoleDesc == 'Researcher') {
            //if application of access is still pending
            requestService.getUnapprovedRequestByUser(User._id).then(function(resp){
              if (resp.data.result != null){
                if(resp.data.result.approve_tag == 'NO'){
                  if(resp.data.result.redirectlink) {
                    $window.location.href= resp.data.result.redirectlink;
                  } else {
                    $window.location.href='#/registration/projecttitle';
                  }
                  // $location.path( '/registration/projecttitle?true' );
                } else {
                }
              } else {
                $window.location.href='#/search';
                // $location.path( '/search?true' );
              }
            });

          } else if(resp.data.roleID.RoleDesc == 'Administrator') {
            $window.location.href='#/admin';
          }
        } else {
          $window.location.href='#/login?login=false';
        }
      });
    }

  });