'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration5Ctrl', function (ENV, $rootScope, $scope, User, $location, requestService, _, growl) {

  	User.loggedIn = true;
    $scope.serverIP = ENV.apiEndpoint;

  	$scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
    });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {
        loadData()
    });

    $scope.popover = {
    };

  	$scope.back = function(){
  		$location.path( '/registration/resourcesrequired' );
  	};

    $scope.onUpload = function(data, document){
      growl.info("Uploading Signature", {ttl: 1000, disableCountDown: true});
    }

    $scope.onComplete = function(resp){
      growl.success("Signature Uploaded", {ttl: 1000, disableCountDown: true});
      loadData();
    }
    
    function loadData(){

      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        $scope.request = resp.data.result[0];
        $scope.request.redirectlink = window.location.hash;
        growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

        $scope.minDate = new moment().subtract(1, 'days');
        if($scope.request.certificationbyapplicant.c_date == undefined){
          $scope.request.certificationbyapplicant.c_date = new Date();
        }

        if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
              },
              onopen: function() {
              }
            });
          }
      })

    }

  	$scope.save = function(){

      $scope.saving = true;

      if(validate()){
        growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

        requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
          $scope.saving = false;
          growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
        })
      } else {
        $scope.saving = false;
        growl.error("Please enter required fields before saving.", {ttl: 1000, disableCountDown: true});
      }

  	};

  	$scope.next = function(){
      if(validate()){
        $scope.save();
  		  $location.path( '/registration/applicantchecklist' );
      } else {
        growl.error("Please enter required fields and save before continue.", {ttl: 1000, disableCountDown: true});
      }
  	};

    function validate(){

      $scope.validateihaveread();
      $scope.validateicertify_information();
      $scope.validateiunderstand();
      $scope.validateicertify_requested();

      $scope.validateFirstName();
      $scope.validateLastName();
      // $scope.validateSignature()
      $scope.validateDate();

      if(
        !$scope.validateihaveread_error &&
        !$scope.validateicertify_information_error &&
        !$scope.validateiunderstand_error &&
        !$scope.validateicertify_requested_error &&

        !$scope.certificationbyapplicantcfname_error &&
        !$scope.certificationbyapplicantclname_error &&
        // !$scope.certificationbyapplicantcsignature_error &&
        !$scope.certificationbyapplicantcdate_error
      ){
        return true;
      } else {
        return false;
      }

      return false;

    }

    $scope.validateihaveread = function(){
      $scope.validateihaveread_error = false;
      if(!$scope.request.certification_page.ihaveread){
        $scope.validateihaveread_error = true;
        growl.error("Please check if you have read the BREAST Access and Management policy document and accept the terms of access outlined in the document.", {ttl: 3000, disableCountDown: true});
      }
    }

    $scope.validateicertify_information = function(){
      $scope.validateicertify_information_error = false;
      if(!$scope.request.certification_page.icertify_information){
        $scope.validateicertify_information_error = true;
        growl.error("Please check if you certify that the information I have provided in this form is accurate and true.", {ttl: 3000, disableCountDown: true});
      }
    }

    $scope.validateiunderstand = function(){
      $scope.validateiunderstand_error = false;
      if(!$scope.request.certification_page.iunderstand){
        $scope.validateiunderstand_error = true;
        growl.error("Please check if you understand that I must acknowledge the BREAST platform and its funders in publications and presentations that arise from this database.", {ttl: 3000, disableCountDown: true});
      }
    }

    $scope.validateicertify_requested = function(){
      $scope.validateicertify_requested_error = false;
      if(!$scope.request.certification_page.icertify_requested){
        $scope.validateicertify_requested_error = true;
        growl.error("Please check if you certify that the requested data will only be used for the project outlined in this application and for no other purpose.", {ttl: 3000, disableCountDown: true});
      }
    }


    $scope.validateFirstName = function(){
      $scope.certificationbyapplicantcfname_error = false

      if(validator.isNull($scope.request.certificationbyapplicant)){
        $scope.certificationbyapplicantcfname_error = true;
        growl.error("Please enter First Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.certificationbyapplicant.c_fname)){
          $scope.certificationbyapplicantcfname_error = true;
          growl.error("Please enter First Name", {ttl: 1000, disableCountDown: true});
        } else {
          if(/\d/.test($scope.request.certificationbyapplicant.c_fname)){
            $scope.certificationbyapplicantcfname_error = true
            growl.error("First Name should only contain alphabet characters", {ttl: 1000, disableCountDown: true});
          }
        }
      }
    }

    $scope.validateLastName = function(){
      $scope.certificationbyapplicantclname_error = false

      if(validator.isNull($scope.request.certificationbyapplicant)){
        $scope.certificationbyapplicantclname_error = true;
        growl.error("Please enter Last Namse", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.certificationbyapplicant.c_lname)){
          $scope.certificationbyapplicantclname_error = true;
          growl.error("Please enter Last Name", {ttl: 1000, disableCountDown: true});
        } else {
          if(/\d/.test($scope.request.certificationbyapplicant.c_lname)){
            $scope.certificationbyapplicantclname_error = true
            growl.error("Last Name should only contain alphabet characters", {ttl: 1000, disableCountDown: true});
          }
        }
      }
    }

    $scope.validateSignature = function(){
      $scope.certificationbyapplicantcsignature_error = false

      if(validator.isNull($scope.request.certificationbyapplicant)){
        $scope.certificationbyapplicantcsignature_error = true;
        growl.error("Please enter Last Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.certificationbyapplicant.c_signature)){
          $scope.certificationbyapplicantcsignature_error = true;
          growl.error("Please enter Last Name", {ttl: 1000, disableCountDown: true});
        }
      }
    }

    $scope.validateDate = function(){
      $scope.certificationbyapplicantcdate_error = false

      if(validator.isNull($scope.request.certificationbyapplicant)){
        $scope.certificationbyapplicantcdate_error = true;
        growl.error("Please enter Date", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.certificationbyapplicant.c_date)){
          $scope.certificationbyapplicantcdate_error = true;
          growl.error("Please enter Date", {ttl: 1000, disableCountDown: true});
        }
      }
    }

  });
