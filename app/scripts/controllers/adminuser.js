'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminRequestCtrl
 * @description
 * # AdminRequestCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminUserCtrl', function (ENV, $scope, User, $modal, $stateParams, userService, growl, $window) {
    User.loggedIn = true;
  	$scope.user = User;
    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
      growl.info("Loading User", {ttl: 1000, disableCountDown: true});
      $scope.requestid = $stateParams.id;

      $scope.cases = {};
      $scope.scores = {};

      userService.getUser($scope.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.currentuser = resp.data.result;


          growl.success("User Loaded", {ttl: 1000, disableCountDown: true});
        }
      });

    });

    $scope.deleteUser = function(){
      var deletethis = confirm("Are you sure you want to delete this user?");
      if (deletethis == true) {
          userService.deleteUser($scope.user._id).then(function(resp){
            
            $window.location.href='#/admin/users';
          })
      } else {
      }
    }


  });
