'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration1Ctrl
 * @description
 * # Registration1Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration1Ctrl', function ($rootScope, $scope, User, requestService, userService, addressService, $location, growl) {

  	User.loggedIn = true;

  	$scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
      $scope.suburbs = []; 
      $scope.cities = [];
	  });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
      requestidempty();
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {
      
      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];
          $scope.request.redirectlink = window.location.hash;
          
          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

          if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
                closeAdminMessage();
              },
              onopen: function() {
              }
            });
          }

        }
      })

    });

    $scope.suburbChange = function(){
      addressService.getSuburbs($scope.request.suburb).then(function(resp){
        $scope.suburbs = resp.data;
        $scope.suburbsnames = _.pluck($scope.suburbs, 'name');
      })
    }

    $scope.citiesChange = function(){
      addressService.getCities().then(function(resp){
        $scope.cities = resp.data.result;
        $scope.citiesnames = _.pluck($scope.cities, 'name');
      })
    }

    $scope.$on('$typeahead.select', function(value, index){
      var record = _.find($scope.suburbs, function(suburb) {
        return suburb.name == index;
      });
      try {
        $scope.request.postcode = record.postcode;
        $scope.request.state = record.state.abbreviation;
      }
      catch(e){

      }
    });

    function closeAdminMessage(){
      $scope.request.message = "";
      requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
      })
    }

    var requestidempty = $rootScope.$on('requestidempty', function(event, mass) {
      $location.path( '/search' );
    });

  	$scope.save = function(){

      $scope.saving = true;
      if(validate()){
        growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

        requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
          $scope.saving = false;
          growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
        })
      } else {
        $scope.saving = false;
        growl.error("Please enter required fields before saving.", {ttl: 1000, disableCountDown: true});
      }
      
  	};

  	$scope.next = function(){
      if(validate()){
        $scope.save();
  		  $location.path( '/registration/researchprojectinformation' );
      } else {
        growl.error("Please enter required fields and save before continue.", {ttl: 1000, disableCountDown: true});
      }
  	};

    function validate(){   

    $scope.validateProjectTitle()
    $scope.validateResearcherTitle()
    $scope.validateResearcherFirstName()
    $scope.validateResearcherLastName()
    $scope.validateDepartment()
    $scope.validateInstitution()
    $scope.validatePostalAddress()
    $scope.validateSuburb()
    // $scope.validateState()
    $scope.validatePostCode()
    $scope.validatePhone()
    // $scope.validateFax()
    $scope.validateMobile()
    $scope.validateEmail()

      if( !$scope.project_title_error &&
        !$scope.u_title_error &&
        !$scope.u_fname_error &&
        !$scope.u_lname_error &&
        !$scope.department_error &&
        !$scope.institution_error &&
        !$scope.postaladdress_error &&
        !$scope.suburb_error &&
        // !$scope.state_error &&
        !$scope.postcode_error &&
        !$scope.phone_error &&
        // !$scope.fax_error &&
        !$scope.mobile_error &&
        !$scope.email_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateProjectTitle = function(){

      $scope.project_title_error = false

      if(validator.isNull($scope.request.project_title)){
        $scope.project_title_error = true;
        growl.error("Please enter Project Title", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateResearcherTitle = function(){

      $scope.u_title_error = false

      if(validator.isNull($scope.request.u_title)){
        $scope.u_title_error = true
        growl.error("Please enter Researcher Title", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateResearcherFirstName = function(){

      $scope.u_fname_error = false

      if(validator.isNull($scope.request.u_fname)){
        $scope.u_fname_error = true
        growl.error("Please enter First Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.request.u_fname)){
          $scope.u_fname_error = true
          growl.error("First Name should only contain alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }

    }

    $scope.validateResearcherLastName = function(){

      $scope.u_lname_error = false

      if(validator.isNull($scope.request.u_lname)){
        $scope.u_lname_error = true
        growl.error("Please enter Last Name", {ttl: 1000, disableCountDown: true});
      } else {
        if(/\d/.test($scope.request.u_fname)){
          $scope.u_lname_error = true
          growl.error("Last Name should only contain alphabet characters", {ttl: 1000, disableCountDown: true});
        }
      }

    }

    $scope.validateDepartment = function(){

      $scope.department_error = false

      if(validator.isNull($scope.request.department)){
        $scope.department_error = true
        growl.error("Please enter Department Name", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateInstitution = function(){

      $scope.institution_error = false

      if(validator.isNull($scope.request.institution)){
        $scope.institution_error = true
        growl.error("Please enter Institution Name", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validatePostalAddress = function(){

      $scope.postaladdress_error = false

      if(validator.isNull($scope.request.postaladdress)){
        $scope.postaladdress_error = true
        growl.error("Please enter Postal Address", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validateSuburb= function(){

      $scope.suburb_error = false

      if(validator.isNull($scope.request.suburb)){
        $scope.suburb_error = true
        growl.error("Please enter Suburb", {ttl: 1000, disableCountDown: true});
      } else {
        // if(!validator.isAlpha($scope.request.suburb)){
        //   $scope.suburb_error = true
        //   growl.error("Suburb should only contain alphabet characters", {ttl: 1000, disableCountDown: true});
        // }
      }

    }

    $scope.validateState= function(){

      $scope.state_error = false

      if(validator.isNull($scope.request.state)){
        $scope.state_error = true
        growl.error("Please select a state", {ttl: 1000, disableCountDown: true});
      } else {
      }

    }

    $scope.validatePostCode= function(){

      $scope.postcode_error = false

      if(validator.isNull($scope.request.postcode)){
        $scope.postcode_error = true;
        growl.error("Please enter postcode", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isInt($scope.request.postcode)){
          $scope.postcode_error = true;
          growl.error("Please enter correct postcode", {ttl: 1000, disableCountDown: true});
        }
      }

    }

    $scope.validatePhone= function(){

      $scope.phone_error = false

      if(validator.isNull($scope.request.phone)){
        $scope.phone_error = true
        growl.error("Please enter phone number", {ttl: 1000, disableCountDown: true});
      } else {
        if(!($scope.request.phone).match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)){
          $scope.phone_error = true
          growl.error("Please enter a valid Phone Number", {ttl: 1000, disableCountDown: true});
        }
      }

    }

    // $scope.validateFax= function(){

    //   $scope.fax_error = false

    //   if(validator.isNull($scope.request.fax)){
    //     $scope.fax_error = true
    //     growl.error("Please enter a fax number", {ttl: 1000, disableCountDown: true});
    //   } else {
    //     if(!($scope.request.fax).match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)){
    //       $scope.fax_error = true
    //       growl.error("Please enter a valid Fax Number", {ttl: 1000, disableCountDown: true});
    //     }
    //   }

    // }  

    $scope.validateMobile= function(){

      $scope.mobile_error = false

      if(validator.isNull($scope.request.mobile)){
        $scope.mobile_error = true
        growl.error("Please enter mobile phone number", {ttl: 1000, disableCountDown: true});
      } else {
        if(!($scope.request.mobile).match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)){
          $scope.mobile_error = true
          growl.error("Please enter a valid mobile phone number", {ttl: 1000, disableCountDown: true});
        }
      }

    } 

    $scope.validateEmail= function(){

      $scope.email_error = false

      if(validator.isNull($scope.request.email)){
        $scope.email_error = true
        growl.error("Please enter email", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isEmail($scope.request.email)){
          $scope.email_error = true
          growl.error("Please enter a valide email", {ttl: 1000, disableCountDown: true});
        } else {

        }
      }

    }   


  });
