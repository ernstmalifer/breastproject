'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration4Ctrl', function ($rootScope, $scope, User, $location, requestService, testSetsService, growl) {

  	User.loggedIn = true;

  	$scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
      testSetsService.getTestSetsColumns().then(function(response){
        $scope.columnNames = response.data.result;
      })
      testSetsService.getScoresColumns().then(function(response){
        $scope.scoresColumnNames = response.data.result;
      })
    });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {

      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];
          $scope.request.redirectlink = window.location.hash;

          _.each($scope.request.breastresources.br_readerdata, function(value, key, obj) {
            if(!_.isEmpty(value)){
              var b = {};
              b[key] = value;
              $scope.cases = _.extend($scope.cases, b);
            }
          });

          _.each($scope.request.breastresources.br_scoredata, function(value, key, obj) {
            if(!_.isEmpty(value)){
              var b = {};
              b[key] = value;
              $scope.scores = _.extend($scope.scores, b);
            }
          });

          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

          if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
              },
              onopen: function() {
              }
            });
          }
        }
      })
    });

    $scope.cases = {};
    $scope.caseToAdd = {
      criteria: '',
      value: []
    }

    $scope.check = function(criteria){
      var column = _.find($scope.columnNames, function(columnName){ return columnName._id == criteria; });
      return column.value;
    }

    $scope.columnValues = [];

    $scope.loadValues = function(){
      $scope.caseToAdd.value = [];

      growl.info("Loading Values", {ttl: 1000, disableCountDown: true});

      testSetsService.getTestSetsColumnValue($scope.caseToAdd.criteria).then(function(response){
         $scope.columnValues = response.data.result;
         growl.success("Values Loaded", {ttl: 1000, disableCountDown: true});
      })
    }

    $scope.toggleSelection = function toggleSelection(value) {

      var idx = $scope.caseToAdd.value.indexOf(value);

      if (idx > -1) {
        $scope.caseToAdd.value.splice(idx, 1);
      }

      else {
        $scope.caseToAdd.value.push(value);
      }
    };

    $scope.addCase = function(){
      if($scope.caseToAdd.criteria != '' && !_.isEmpty($scope.caseToAdd.value)){
        $scope.cases[$scope.caseToAdd.criteria] = $scope.caseToAdd.value;
        $scope.caseToAdd = {
          criteria: '',
          value: []
        }
      } else {
        growl.error("Please enter a case", {ttl: 1000, disableCountDown: true});
      }

    };

    $scope.removeCase = function(otherCase) {
      $scope.cases = _.omit($scope.cases, otherCase)
      // console.log($scope.cases)
    }

    /* ------------------------ */

    $scope.scores = {};
    $scope.scoreToAdd = {
      criteria: '',
      value: []
    }

    $scope.checks = function(criteria){
      var column = _.find($scope.scoresColumnNames, function(columnName){ return columnName._id == criteria; });
      // console.log($scope.scoresColumnNames, column)
      return column.value;
    }

    $scope.scoreColumnValues = [];

    $scope.loadScoreValues = function(){
      $scope.scoreToAdd.value = [];

      growl.info("Loading Values", {ttl: 1000, disableCountDown: true});

      testSetsService.getScoresColumnValue($scope.scoreToAdd.criteria).then(function(response){
         $scope.scoreColumnValues = response.data.result;

         $scope.scoreColumnValues = _.filter($scope.scoreColumnValues, function(scoreColumnValue){
          return !scoreColumnValue.match(/None/g);
         });

         growl.success("Values Loaded", {ttl: 1000, disableCountDown: true});
      })
    }

    $scope.toggleScoreSelection = function toggleSelection(value) {

      var idx = $scope.scoreToAdd.value.indexOf(value);

      if (idx > -1) {
        $scope.scoreToAdd.value.splice(idx, 1);
      }

      else {
        $scope.scoreToAdd.value.push(value);
      }
    };

    $scope.addScore = function(){
      if($scope.scoreToAdd.criteria != '' && !_.isEmpty($scope.scoreToAdd.value)){
        $scope.scores[$scope.scoreToAdd.criteria] = $scope.scoreToAdd.value;
        $scope.scoreToAdd = {
          criteria: '',
          value: []
        }
      } else {
        growl.error("Please enter a score", {ttl: 1000, disableCountDown: true});
      }
    };

    $scope.removeScore = function(otherCase) {
      $scope.scores = _.omit($scope.scores, otherCase)
    }


    // $scope.request.additionalquestionnaires = [];
    $scope.otherAdditionalQuestionToAdd = {
      question: '',
      description: ''
    }

    $scope.addAdditionalQuestion = function(){
      console.log($scope.otherAdditionalQuestionToAdd)
      if($scope.otherAdditionalQuestionToAdd.question != '' && $scope.otherAdditionalQuestionToAdd.description != ''){
        $scope.request.additionalquestionnaires.push($scope.otherAdditionalQuestionToAdd);
        clearOtherAdditionalQuestion();
      } else {
        growl.error("Please enter a question and a description before adding.", {ttl: 1000, disableCountDown: true});
      }
    };

    $scope.removeAdditionalQuestion = function(otherAdditionalQuestion) {
      // console.log(otherResearcher)
      $scope.request.additionalquestionnaires.splice(otherAdditionalQuestion, 1)
    }

    function clearOtherAdditionalQuestion(){
      $scope.otherAdditionalQuestionToAdd = {
        question: '',
        description: ''
      }
    }

  	$scope.back = function(){
  		$location.path( '/registration/funding' );
  	};

  	$scope.save = function(){

      $scope.saving = true;

      if(validate()){
        growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

        $scope.request.breastresources.br_readerdata = {};
        $scope.request.breastresources.br_scoredata = {};

        // yawa = _.each(yawa, function(value, key){
        //   value = [];
        //   console.log(key, value)
        //   // readerdata = [];
        //   // console.log(readerdata)
        // })

        // $scope.request.breastresources.br_readerdata = angular.copy(_.each($scope.request.breastresources.br_readerdata, function(value, key){
        //   value = [];
        //   console.log(value, key)
        //   // readerdata = [];
        //   // console.log(readerdata)
        // }))
        // $scope.request.breastresources.br_scoredata = _.each($scope.request.breastresources.br_scoredata, function(scoredata){
        //   scoredata = [];
        // })

        // console.log($scope.request.breastresources.br_readerdata)
        // console.log($scope.request.breastresources.br_readerdata)

        $scope.request.breastresources.br_readerdata = _.extend($scope.request.breastresources.br_readerdata, angular.copy($scope.cases));
        $scope.request.breastresources.br_scoredata = _.extend($scope.request.breastresources.br_scoredata, angular.copy($scope.scores));


        requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
          $scope.saving = false;
          growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
        })
      } else {
        $scope.saving = false;
        growl.error("Please enter required fields before saving.", {ttl: 1000, disableCountDown: true});
      }
  	};

  	$scope.next = function(){
      if(validate()){
        $scope.save();
  		  $location.path( '/registration/certificationbyapplicant' );
      } else {
        growl.error("Please enter required fields and save before continue.", {ttl: 1000, disableCountDown: true});
      }
  	};

    function validate(){

      $scope.validateCasesScores()
      // $scope.validateCases()
      // $scope.validateScores()
      // $scope.validateParticipantsNumber()

      if( !$scope.casesscores_error
        // !$scope.cases_error &&
        // !$scope.scores_error /*&&*/
        // !$scope.br_numberofparticipants_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateCasesScores = function(){

      $scope.casesscores_error = false

      // console.log(_.isEmpty($scope.cases), _.isEmpty($scope.scores))

      if(_.isEmpty($scope.cases) && _.isEmpty($scope.scores)){
        $scope.casesscores_error = true;
        growl.error("Please add at least either or both case details and scores data", {ttl: 1000, disableCountDown: true});
      } else {
      }

      // $scope.scores_error = false

      // if(_.isEmpty($scope.scores)){
      //   $scope.scores_error = true;
      //   growl.error("Please add at least one Reader Score", {ttl: 1000, disableCountDown: true});
      // } else {
      // }

    }


    $scope.validateCases = function(){

      $scope.cases_error = false

      if(_.isEmpty($scope.cases)){
        $scope.cases_error = true;
        growl.error("Please add at least one Case", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateScores = function(){

      $scope.scores_error = false

      if(_.isEmpty($scope.scores)){
        $scope.scores_error = true;
        growl.error("Please add at least one Reader Score", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateParticipantsNumber = function(){

      $scope.br_numberofparticipants_error = false

      if(validator.isNull($scope.request.breastresources.br_numberofparticipants)){
        $scope.br_numberofparticipants_error = true;
        growl.error("Please enter Participants Number", {ttl: 1000, disableCountDown: true});
      } else {
        if(!validator.isInt($scope.request.breastresources.br_numberofparticipants, { min: 0})){
          $scope.br_numberofparticipants_error = true;
          growl.error("Particpants Number should be a number", {ttl: 1000, disableCountDown: true});
        }
      }

    }

  });
