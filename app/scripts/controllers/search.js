// 'use strict'; // safari BUG

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('SearchCtrl', function ($rootScope, $scope, User, $location, _, testSetsService, $modal, $timeout, ENV, growl, userService, Pagination) {

    User.loggedIn = true;
    $scope.apiEndpoint = ENV.apiEndpoint;

    $scope.rdatasFilteredSearch = [];
    $scope.cdatasFilteredSearch = [];

    $scope.numberOfPages = function(){
      if($scope.searchQuery.dataType == 'Readings' || $scope.searchQuery.dataType == 'Cases') {
        return Math.ceil($scope.rdatasFilteredSearch.length/$scope.pageSize);
      } else {
        return Math.ceil($scope.cdatasFilteredSearch.length/$scope.pageSize);
      }
    }

    $scope.lengthResults = function(){
      if($scope.searchQuery.dataType == 'Readings' || $scope.searchQuery.dataType == 'Cases') {
        return $scope.rdatasFilteredSearch.length;
      } else {
        return $scope.cdatasFilteredSearch.length;
      }
    }

    $scope.$on('$viewContentLoaded', function() {
      testSetsService.getTestSetsColumns().then(function(response){
        $scope.columnNames = response.data.result;
        $scope.columnNames = _.map($scope.columnNames, function(columnName){ return {name: columnName, selected: false} });
      })
      testSetsService.getScoresColumns().then(function(response){
        $scope.scoresColumnNames = response.data.result;
        $scope.scoresColumnNames = _.map($scope.scoresColumnNames, function(columnName){ return {name: columnName, selected: false} });
      })

      testSetsService.getTestSetsColumnValue('Test_set').then(function(response){
        $scope.testSets = response.data.result;

        $scope.testSets = _.map($scope.testSets, function(testSet){ return {'label': testSet, 'selected': true} });

      })

      $scope.dataTypes = [
        {value:'Readings', label: 'Case Details'},
        // {value:'Cases', label: 'Case Images'},
        {value:'Scores', label: 'Performance Data'}
      ]
      $scope.predicate = 'Test_set';
      $scope.currentPage = 0;
      $scope.pageSize = 30
      $scope.reverse = true;
      $scope.downloadType = {
        name: 'email'
      }

      $scope.pagination = Pagination.getNew(30);

    });

    var requestidloaded = $rootScope.$on('userprofileloaded', function(event, mass) {
      $scope.email = User.u_email;
    });

    $scope.setPredicate = function(predicate){
      $scope.predicate = predicate;
      $scope.reverse = !$scope.reverse;
    }

    $scope.sortCaret = function(predicate){
      if ($scope.predicate == predicate) return true; else return false;
    }

    $scope.sortCaretPosition = function(){
      if ($scope.reverse) return ''; else return 'dropup';
    }

    $scope.searchQuery = {
      testSet: [],
      dataType: 'Readings',
      columnName: [],
      searchkeyword: ''
    };

    // $scope.searchQuery = {"dataType":"Readings","columnName":[{"name":"Breast_Density","selected":true,"values":[{"label":"Heterogenous  (50-75% glandular)","selected":true}]},{"name":"Cancer_Type","selected":true,"values":[{"label":"Spiculated Mass","selected":true}]}],"searchkeyword":""}

    testSetsService.getTestsets().then(function(resp){
      $scope.rdatas = resp.data.result[0];
      $scope.cdatas = resp.data.result[1];

      $scope.changeDataType();

      $scope.rdatasFilteredSearch = angular.copy($scope.rdatas);
      $scope.cdatasFilteredSearch = angular.copy($scope.cdatas);
    })

    function rdataload(){
      testSetsService.getTestSetsColumns().then(function(response){
        $scope.columnNames = response.data.result;
        $scope.columnNames = _.map($scope.columnNames, function(columnName){ return {name: columnName, selected: false} });
      

      $scope.rdatascolumns = [];
      _.each($scope.rdatas, function(rdata){
        $scope.rdatascolumns = _.union($scope.rdatascolumns, _.keys(rdata))
      })

      $scope.rdatas = _.each($scope.rdatas, function(rdata){
        _.each($scope.rdatascolumns, function(rdatascolumn){
          if(!_.has(rdata, rdatascolumn)){
            var o = {};
            o[rdatascolumn] = '';
            rdata = _.extend(rdata, o);
            rdata = _.extend(rdata, {selected: false});
          }
        })
        rdata['Thumbnail'] = rdata['DICOM_folder_ID']

        if(rdata['download_link']){
          rdata['Link'] = rdata['download_link']
        }
      });

      $scope.rdatascolumns = _.map($scope.rdatascolumns, function(rdatascolumn){ return {'label': rdatascolumn, 'selected': false, value: $scope.check(rdatascolumn)} });

      if(_.find($scope.rdatascolumns, function(column){ return column.label == 'Thumbnail'; }) == undefined){
        $scope.rdatascolumns.push({label: 'Thumbnail', selected: true, value: 'Thumbnail'})
      }


      var defaultColumnsSelected = ["Test_set", "Breast_Density", "Cancer_Type", "Size"];
      $scope.rdatascolumns = _.each($scope.rdatascolumns, function(rdatascolumn){ if( _.indexOf(defaultColumnsSelected, rdatascolumn.label) != -1 ){ rdatascolumn.selected = true; } })


      if(_.find($scope.rdatascolumns, function(column){ return column.label == 'Link'; }) == undefined && User.role == 'Administrator'){
        $scope.rdatascolumns.push({label: 'Link', selected: true, value: 'Link'})
      }

      $scope.rdatasFilteredSearch = angular.copy($scope.rdatas);

      })

    }

    $scope.check = function(rdatascolumn){
      var column = _.find($scope.columnNames, function(columnName){ return columnName.name._id == rdatascolumn; });
      // console.log(column)
      if(column){
        return column.name.value;
      } else {
        return rdatascolumn
      }
    }

    function cdataload(){
      testSetsService.getScoresColumns().then(function(response){
        $scope.scoresColumnNames = response.data.result;
        $scope.scoresColumnNames = _.map($scope.scoresColumnNames, function(columnName){ return {name: columnName, selected: false} });
      
      $scope.cdatascolumns = [];
      _.each($scope.cdatas, function(cdata){
        $scope.cdatascolumns = _.union($scope.cdatascolumns, _.keys(cdata))
      })

      $scope.cdatas = _.each($scope.cdatas, function(cdata){
        _.each($scope.cdatascolumns, function(cdatascolumn){
          if(!_.has(cdata, cdatascolumn)){
            var o = {};
            o[cdatascolumn] = '';
            cdata = _.extend(cdata, o);
            cdata = _.extend(cdata, {selected: false});
          }
        })
      });

      $scope.cdatascolumns = _.map($scope.cdatascolumns, function(cdatascolumn){ return {'label': cdatascolumn, 'selected': false, value: $scope.checkc(cdatascolumn)} });

      var defaultColumnsSelected = ["Test_set", "Use_of_corrective_lenses", "Left_Right_handed"];
      $scope.cdatascolumns = _.each($scope.cdatascolumns, function(cdatascolumn){ if( _.indexOf(defaultColumnsSelected, cdatascolumn.label) != -1 ){ cdatascolumn.selected = true; } })

      $scope.cdatasFilteredSearch = angular.copy($scope.cdatas);
      })
    }

    $scope.checkc = function(cdatascolumn){
      var column = _.find($scope.scoresColumnNames, function(columnName){ return columnName.name._id == cdatascolumn; });
      if(column){
        return column.name.value;
      } else {
        return cdatascolumn
      }
    }

    $scope.changeDataType = function(){
      switch($scope.searchQuery.dataType){
        case 'Readings':
          rdataload();
          break;
        case 'Cases':
          rdataload();
          break;
        case 'Scores':
          cdataload();
          break;
      }

      $scope.columnNames = _.each($scope.columnNames, function(columnName){ columnName.selected = false });
      $scope.scoresColumnNames = _.each($scope.scoresColumnNames, function(scoresColumnNames){ scoresColumnNames.selected = false });
      $scope.searchQuery.columnName = [];

    }

    $scope.isSelected = function(key){
      switch($scope.searchQuery.dataType){
        case 'Readings':
          var columnkey = _.find($scope.rdatascolumns, function(rdatascolumn){
            if(rdatascolumn.label == key){
              // console.log(key, rdatascolumn)
              return true;
            }
          });
          if(columnkey){
            return columnkey.selected;
          } else {
            return false;
          }
          break;
        case 'Cases':
          break;
        case 'Scores':
          var columnkey = _.find($scope.cdatascolumns, function(cdatascolumn){
            if(cdatascolumn.label == key){
              // console.log(key, rdatascolumn)
              return true;
            }
          });
          if(columnkey){
            return columnkey.selected;
          } else {
            return false;
          }
          break;
      }
    }

    $scope.selectAllCheck = {value: false};
    $scope.selectAll = function(){
      if($scope.selectAllCheck.value){
        $scope.rdatasFilteredSearch = _.each($scope.rdatasFilteredSearch, function(rdata){ rdata.selected = true; });
        $scope.cdatasFilteredSearch = _.each($scope.cdatasFilteredSearch, function(cdata){ cdata.selected = true; });
      } else {
        $scope.rdatasFilteredSearch = _.each($scope.rdatasFilteredSearch, function(rdata){ rdata.selected = false; });
        $scope.cdatasFilteredSearch = _.each($scope.cdatasFilteredSearch, function(cdata){ cdata.selected = false; });
      }
    }

    $scope.loadValues = function(){

      $scope.moreSelected = 0;

      $scope.searchQuery.columnName = _.filter($scope.columnNames, function(columnName){
        return columnName.selected
      });

      $scope.columnValues = []

      _.each($scope.searchQuery.columnName, function(columnNameEach){
        testSetsService.getTestSetsColumnValue(columnNameEach.name._id).then(function(response){
          $scope.columnNameEachEach = _.map(response.data.result, function(result){ return {label: result, selected: false} })
          $scope.columnValues.push({name: columnNameEach.name, columns: $scope.columnNameEachEach});
        })
      })

    }

    $scope.loadScoreValues = function(){

      $scope.moreSelected = 0;

      $scope.searchQuery.columnName = _.filter($scope.scoresColumnNames, function(columnName){
        return columnName.selected
      });

      $scope.columnValues = []

      _.each($scope.searchQuery.columnName, function(columnNameEach){
        testSetsService.getScoresColumnValue(columnNameEach.name._id).then(function(response){
          $scope.columnNameEachEach = _.map(response.data.result, function(result){ return {label: result, selected: false} })
          $scope.columnValues.push({name: columnNameEach.name, columns: $scope.columnNameEachEach});
        })
      })

    }

    $scope.setcolumnValuesPopover = function(columnNameName){

      // if($scope.searchQuery.dataType == 'Readings' || $scope.searchQuery.dataType == 'Cases'){
      $scope.columnValuesPopover = _.filter($scope.columnValues, function(columnValue){
        return columnValue.name._id == columnNameName._id 
      })
    }

    $scope.download = function(){

      var notif;

      if( $scope.downloadType.name == 'zip') {
        notif = growl.info("Downloading files, links for the images will be sent into your email", {ttl: -1, disableCountDown: true});
      } else if($scope.downloadType.name == 'email'){
        notif = growl.info("Sending files to your email", {ttl: -1, disableCountDown: true});
      } else if($scope.downloadType.name == 'googledrive'){
        notif = growl.info("Downloading files to your Google Drive", {ttl: -1, disableCountDown: true});
      } else if($scope.downloadType.name == 'dropbox'){
        notif = growl.info("Downloading files to your Dropbox", {ttl: -1, disableCountDown: true});
      }

      var selectedTestSetsIDS = [];
      var selectedColumns = [];
      switch($scope.searchQuery.dataType){
        case 'Readings':
          selectedTestSetsIDS = _.pluck(_.filter($scope.rdatasFilteredSearch, function(rdata){return rdata.selected}), '_id');
          selectedColumns = _.pluck(_.filter($scope.rdatascolumns, function(rdatacol){return rdatacol.selected}), 'label');
          break;
        case 'Cases':
          selectedTestSetsIDS = _.pluck(_.filter($scope.rdatasFilteredSearch, function(rdata){return rdata.selected}), '_id');
          break;
        case 'Scores':
          selectedTestSetsIDS = _.pluck(_.filter($scope.cdatasFilteredSearch, function(cdata){return cdata.selected}), '_id');
          selectedColumns = _.pluck(_.filter($scope.cdatascolumns, function(cdatacol){return cdatacol.selected}), 'label');
          break;
      }

      testSetsService.download(User._id, selectedTestSetsIDS, $scope.downloadType.name, $scope.searchQuery.dataType, $scope.email, selectedColumns, $scope.downloadType.imagetype ).then(function(resp){

        notif.destroy();

        if($scope.downloadType.name == 'zip') {
          // saveData(data, 'download.zip');
          // saveAs(data, "download.zip");
          // window.open(resp.)
          // console.log(User._id)
          $.fileDownload(ENV.apiEndpoint + '/public/tmp/' +User._id+ '/' + resp.data.name, {
            successCallback: function(url){
              console.log('downloaded')
            },
            failCallback: function(html, url){
              console.log('error')
            }
          });
          // window.open(ENV.apiEndpoint + '/public/tmp/' + User._id + '.zip')
        }
        else if($scope.downloadType.name == 'email'){
          if (resp.data.msg == "The file size exceeds the limit, Alllowed only 20MB less") {
            growl.error("The file size exceeds the limit, allowed only 20MB less", {ttl: 5000, disableCountDown: true});
          } else {
            growl.success("The download link for the files are already sent to your email, also please check your spam folder if the email is marked as spam", {ttl: 5000, disableCountDown: true});
          }
        }
        else if($scope.downloadType.name == 'googledrive'){
          growl.success("The google drive files link are already sent to your email", {ttl: 5000, disableCountDown: true});
        }
        else if($scope.downloadType.name == 'dropbox'){
          growl.success("The dropbox files link are already sent to your email", {ttl: 5000, disableCountDown: true});
        }
      })

    }

    $scope.back = function(){
      window.history.back();
    };

    $scope.save = function(){
    };

    $scope.next = function(){
      $location.path( '' );
    };

    // var saveData = (function () {
    //     var a = document.createElement("a");
    //     document.body.appendChild(a);
    //     a.style = "display: none";
    //     return function (data, fileName) {
    //         var datafile = window.URL.createObjectURL(data)
    //         a.href = datafile;
    //         a.download = fileName;
    //         a.click();
    //         window.URL.revokeObjectURL(datafile);
    //     };
    // }());

    $scope.setColumnValues = function(columnValue){
      // console.log(columnValue)
      $scope.searchQuery.columnName = _.each(angular.copy($scope.searchQuery.columnName), function(columnname){
        if(columnname.name._id == $scope.columnValuesPopover[0].name._id){


          if(columnname.values == undefined){
            columnname.values = [];
          }

          if(columnValue.selected){
            columnname.values.push(columnValue)
          } else {
            columnname.values = _.reject(columnname.values, function(value){
              return value.label == columnValue.label
            })
          }
        }
      })
    }

    $scope.changeTestSets = function(){
      $scope.searchQuery.testSet = _.filter($scope.testSets, function(testset){ return testset.selected; });
    }

    $scope.reset = function(){

      $scope.testSets = _.each($scope.testSets, function(testset){ testset.selected = true; })
      console.log($scope.testSets)
      $scope.searchQuery = {
        testSet: _.filter($scope.testSets, function(testset){ return testset.selected; }),
        dataType: 'Readings',
        columnName: [],
        searchkeyword: ''
      };

      $scope.search();
    }

    $scope.search = function(){

      growl.info("Searching...", {ttl: 2000, disableCountDown: true});

      $scope.changeTestSets();

      if(_.isEmpty($scope.searchQuery.testSet)) {
        growl.error("Please select at least one test set", {ttl: 2000, disableCountDown: true});
      }

      if($scope.searchQuery.dataType == 'Readings' || $scope.searchQuery.dataType == 'Cases') {

        $scope.rdatasFilteredSearch = _.filter($scope.rdatas, function(rdata){
          // console.log(_.indexOf(_.pluck($scope.searchQuery.testSet, 'label'), rdata.Test_set), $scope.searchQuery.testSet, rdata.Test_set)
          if( _.indexOf(_.pluck($scope.searchQuery.testSet, 'label'), rdata.Test_set) != -1 ){
            return true;
          }
        });

        $scope.rdatasFilteredSearch = _.filter($scope.rdatasFilteredSearch, function(rdata){

          var pass = [];
          var pass2 = [];
          // Filter Pass
          _.each($scope.searchQuery.columnName, function(columnName){
            if(columnName.values != undefined){
              _.each(columnName.values, function(columnNameValues){
                // console.log(columnNameValues.label, columnName.name._id, rdata[columnName])
                // console.log(columnNameValues.label.replace(/\s+/g, '') == rdata[columnName.name].replace(/\s+/g, ''))
                if(columnNameValues.label.replace(/\s+/g, '') == rdata[columnName.name._id].replace(/\s+/g, '')){
                  pass2.push(true);
                } else {
                  pass2.push(false);
                }
              });
            }

            if(!_.contains(pass2, true)){
              pass.push(true);
            } else {
              pass.push(false);
            }

            pass2 = [];

          })

          if(!_.contains(pass, true)){ // AND
            return true
          }

        })

        $scope.rdatasFilteredSearch = _.filter($scope.rdatasFilteredSearch, function(rdata){

          var pass = [];
          var reg = new RegExp($scope.searchQuery.searchkeyword, 'gi');

          // Contains Text Pass
          _.each(_.values(rdata), function(value){
            // console.log(value.match(reg), value)
            if(String(value).match(reg)){
              pass.push(true);
            }
          });

          if(_.contains(pass, true)){ // AND
            return true
          }

        })

      } else {

        $scope.cdatasFilteredSearch = _.filter($scope.cdatas, function(cdata){
          if( _.indexOf(_.pluck($scope.searchQuery.testSet, 'label'), cdata.Test_set) != -1 ){
            return true;
          }
        });

        $scope.cdatasFilteredSearch = _.filter($scope.cdatasFilteredSearch, function(cdata){

          var pass = [];
          var pass2 = [];
          // Filter Pass
          _.each($scope.searchQuery.columnName, function(columnName){
            if(columnName.values != undefined){
              _.each(columnName.values, function(columnNameValues){
                // console.log(columnNameValues.label,cdata[columnName.name], columnNameValues.label.replace(/\s+/g, '') == cdata[columnName.name].replace(/\s+/g, ''))
                if(columnNameValues.label == cdata[columnName.name._id]){
                  pass2.push(true);
                } else {
                  pass2.push(false);
                }
              });
            }

            if(!_.contains(pass2, true)){
              pass.push(true);
            } else {
              pass.push(false);
            }

            pass2 = [];

          })

          if(!_.contains(pass, true)){ // AND
            return true
          }

          $scope.cdatasFilteredSearch = _.filter($scope.cdatasFilteredSearch, function(cdata){

            var pass = [];
            var reg = new RegExp($scope.searchQuery.searchkeyword, 'gi');

            // Contains Text Pass
            _.each(_.values(cdata), function(value){
              if(String(value).match(reg)){
                pass.push(true);
              }
            });

            if(_.contains(pass, true)){ // AND
              return true
            }

          })

        })
      }

      $scope.currentPage = 0;

    }

    $scope.getNumber = function(num) {
        return new Array(num);
    }

    $scope.setCurrentPage = function(page) {
      $scope.currentPage = page;
      // console.log(page)
    }

    $scope.columnFilter = function(column){
      // console.log(column)
      if(column.label == '__v' || column.label == '_id' || column.label == 'download_link'){
        return false;
      }
      return true;
    }

  });
