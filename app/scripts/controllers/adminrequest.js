'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminRequestCtrl
 * @description
 * # AdminRequestCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminRequestCtrl', function (ENV, $scope, User, $modal, $stateParams, requestService, growl, $window) {
    User.loggedIn = true;
  	$scope.user = User;

    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
      loadRequest()
    });

    function loadRequest(){
      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});
      $scope.requestid = $stateParams.id;

      $scope.cases = {};
      $scope.scores = {};

      requestService.getRequest($scope.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];

          _.each($scope.request.breastresources.br_readerdata, function(value, key, obj) {
            if(!_.isEmpty(value)){
              var b = {};
              b[key] = value;
              $scope.cases = _.extend($scope.cases, b);
            }
          });

          _.each($scope.request.breastresources.br_scoredata, function(value, key, obj) {
            if(!_.isEmpty(value)){
              var b = {};
              b[key] = value;
              $scope.scores = _.extend($scope.scores, b);
            }
          });

          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});
        }
      });
    }

    var approveModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/approveModal.html',
      show: false
    });

    $scope.showApproveModal = function(requestid){
      $scope.requestidtoapprove = requestid;
      approveModal.$promise.then(approveModal.show());
    }

    $scope.approve = function(expirationdate){
      if(expirationdate == undefined) {
        growl.error("Please set an expiry date", {ttl: 1000, disableCountDown: true});
      } else {
        approveModal.hide()
        growl.info("Approving Request", {ttl: 1000, disableCountDown: true});
        console.log($scope.requestidtoapprove, expirationdate)

        requestService.approveRequest($scope.requestidtoapprove, expirationdate).then(function(resp){
          if(resp.data.result != null) {
            growl.success("Request Approved", {ttl: 1000, disableCountDown: true});
            loadRequest()
          }
        })
      }
    }

    $scope.revoke = function(){
      growl.info("Revoking Request", {ttl: 1000, disableCountDown: true});

      requestService.revokeRequest($scope.requestid).then(function(resp){
        if(resp.data.result != null) {

          growl.success("Request Revoked", {ttl: 1000, disableCountDown: true});
          requestService.getRequest($scope.requestid).then(function(resp){
            if(resp.data.result != null) {
              $scope.request = resp.data.result[0];
            }
          })
        }
      })
    }

    var shareModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/shareRequest.html',
      show: false
    });

    $scope.showShareModal = function(requestid) {
      $scope.requestidtoshare = requestid;
      shareModal.$promise.then(shareModal.show());
      // $('#emails').tagsInput({'defaultText':'add email','height':'50px'});
    };

    $scope.shareRequest = function(requestid){
      var email = $('#emails').val();
      growl.info("Sending Request", {ttl: 1000, disableCountDown: true});
      requestService.shareRequest(requestid, email).then(function(resp){
        growl.success("Request Shared to " + email, {ttl: 3000, disableCountDown: true});
      })
    }

    var setExpiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/setExpiryModal.html',
      show: false
    });

    $scope.showSetExpiryModal = function(requestid){
      $scope.requestidtosetexpiry = requestid;
      setExpiryModal.$promise.then(setExpiryModal.show());
    }

    $scope.setExpiry = function(expirationdate){
      growl.info("Setting Expiry Date", {ttl: 1000, disableCountDown: true});

      requestService.setExpiryRequest($scope.requestidtosetexpiry, expirationdate).then(function(resp){
        if(resp.data.result != null) {

          loadRequest()
        }
      })
    }

    var disapproveRequestModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/disapproveRequestModal.html',
      show: false
    });

    $scope.showDisapproveRequestModal = function(requestid){
      $scope.requestidtodisapprove = requestid;
      disapproveRequestModal.$promise.then(disapproveRequestModal.show());
    }

    $scope.disapproveRequest = function(message){
      growl.info("Sending message", {ttl: 1000, disableCountDown: true});

      requestService.disapproveRequest($scope.requestidtodisapprove, message).then(function(resp){

        requestService.getRequests().then(function(resp){
          if(resp.data.result != null) {
            $scope.requests = resp.data.result;
            growl.success("Message sent", {ttl: 1000, disableCountDown: true});
          }
        })
      })
    }

    $scope.deleteRequest = function(){

      var deletethis = confirm("Are you sure you want to delete this request?");
      if (deletethis == true) {
          requestService.deleteRequest($scope.requestid).then(function(resp){

            $window.location.href='#/admin';
          })
      } else {
      }
    }


  });
