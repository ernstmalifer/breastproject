'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminCtrl', function ($scope, User, $modal, requestService, userService, inquiriesService, growl, _) {
    User.loggedIn = true;
  	$scope.user = User;

    $scope.$on('$viewContentLoaded', function() {
      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      $scope.selectAll = false;
      $scope.selectAllI = false;

      // requestService.getRequests().then(function(resp){
      //   if(resp.data.result != null) {
      //     // $scope.pendingRequests = _.filter(resp.data.result, function(request){ return request.approve_tag == 'NO'; });

      //     $scope.requests = resp.data.result;
      //     $scope.requests = _.filter($scope.requests, function(request){ return request.approve_tag == 'NO' });
      //     growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});
      //   }
      // })

    loadData();

      userService.getAllUsers().then(function(resp){
        if (resp.data.result != null) {
          var count = resp.data.result.length;
          data[1].value = count;
          userService.getNoOfUsersLogin().then(function(resp){
            if (resp.data.result != null) {
              var nocount = resp.data.result;
              data[0].value = nocount;
              data[1].value = data[1].value - nocount;

              var ctx = document.getElementById("usersLoggedIn").getContext("2d");
              var myDoughnutChart = new Chart(ctx).Doughnut(data,{});
            }
          })

        }
      })

      

    });

    var data = [
      {
          value: 0,
          color: "#F0453D",
          highlight: "#F6736C",
          label: "Logged In"
      },
      {
          value: 0,
          color:"#CCC",
          highlight: "#EEE",
          label: "Users"
      },
    ]

    var shareModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/shareRequest.html',
      show: false
    });

    $scope.showShareModal = function(requestid) {
      $scope.requestidtoshare = requestid;
      shareModal.$promise.then(shareModal.show());
      // $('#emails').tagsInput({'defaultText':'add email','height':'50px'});
    };

    $scope.shareRequest = function(requestid){
      var email = $('#emails').val();
      growl.info("Sending Request", {ttl: 1000, disableCountDown: true});
      requestService.shareRequest(requestid, email).then(function(resp){
        growl.success("Request Shared to " + email, {ttl: 3000, disableCountDown: true});
      })
    }

    var approveModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/approveModal.html',
      show: false
    });

    $scope.showApproveModal = function(requestid){
      $scope.requestidtoapprove = requestid;
      approveModal.$promise.then(approveModal.show());

      if(requestid == 'selected'){
        $scope.selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      } else {
        $scope.selectedPendingRequests = $scope.getRequest(requestid)
      }
    }

    $scope.approve = function(expirationdate){

      if(expirationdate == undefined) {
        growl.error("Please set an expiry date", {ttl: 1000, disableCountDown: true});
      } else {
        approveModal.hide()
        growl.info("Approving Request", {ttl: 1000, disableCountDown: true});
        console.log($scope.requestidtoapprove, expirationdate)

        if($scope.requestidtoapprove == 'selected'){ //bulk

          var selectedPendingRequests = $scope.getAllSelectedPendingRequest();

          _.each(selectedPendingRequests, function(request){
            requestService.approveRequest(request._id, expirationdate).then(function(resp){
            })
          });

          loadData();
          growl.success("Requests Approved", {ttl: 1000, disableCountDown: true});

        } else {
          requestService.approveRequest($scope.requestidtoapprove, expirationdate).then(function(resp){
            if(resp.data.result != null) {

              loadData();
            }
          })
        }
        
      }

    }

    var setExpiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/setExpiryModal.html',
      show: false
    });

    $scope.showSetExpiryModal = function(requestid){
      $scope.requestidtosetexpiry = requestid;
      setExpiryModal.$promise.then(setExpiryModal.show());
    }

    $scope.setExpiry = function(expirationdate){
      growl.info("Setting Expiry Date", {ttl: 1000, disableCountDown: true});

      var selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      requestService.setExpiryRequest($scope.requestidtosetexpiry, expirationdate).then(function(resp){
        if(resp.data.result != null) {

          loadData();
          growl.success("Expiry Date Set", {ttl: 1000, disableCountDown: true});
        }
      })

    }

    var disapproveRequestModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/disapproveRequestModal.html',
      show: false
    });

    $scope.showDisapproveRequestModal = function(requestid){
      $scope.requestidtodisapprove = requestid;
      disapproveRequestModal.$promise.then(disapproveRequestModal.show());

      if(requestid == 'selected'){
        $scope.selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      } else {
        $scope.selectedPendingRequests = $scope.getRequest(requestid)
      }
    }

    $scope.disapproveRequest = function(message){
      growl.info("Sending message", {ttl: 1000, disableCountDown: true});

      if($scope.requestidtodisapprove == 'selected'){ //bulk

        var selectedPendingRequests = $scope.getAllSelectedPendingRequest();

        _.each(selectedPendingRequests, function(request){
          requestService.disapproveRequest(request._id, message).then(function(resp){
          })
        });

        loadData();
        growl.success("Messages sent", {ttl: 1000, disableCountDown: true});

      } else {
        requestService.disapproveRequest($scope.requestidtodisapprove, message).then(function(resp){

          loadData();
        })
      }
    }

    $scope.getRequest = function(requestid){
      return _.filter($scope.requests, function(request){ return request._id == requestid })
    }

    $scope.hasSelected = function(){
      var selected = $scope.countSelectedPendingRequest()
      if(selected.selected > 0){ return true; }
      else { return false; }
    }

    $scope.selectAllPendingRequests = function(){
      $scope.requests = _.each($scope.requests, function(request){
        request.selected = $scope.selectAll;
      })
    }

    $scope.countSelectedPendingRequest = function(){

      var selected = _.countBy($scope.requests, function(request) {
        return request.selected == true ? 'selected': 'not-selected';
      });

      return selected;
    }

    $scope.getAllSelectedPendingRequest = function(){
      return _.filter($scope.requests, function(request){
        return request.selected == true;
      })
    }

    function loadData(){
      requestService.getRequests().then(function(resp){
        if(resp.data.result != null) {
          // $scope.pendingRequests = _.filter(resp.data.result, function(request){ return request.approve_tag == 'NO'; });

          $scope.requests = resp.data.result;
          $scope.requests = _.filter($scope.requests, function(request){ return request.approve_tag == 'NO' });
          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});
        }
      })

      inquiriesService.getInquiries().then(function(resp){
        if(resp.data.result != null) {
          $scope.inquiries = resp.data.result;
          $scope.inquiries = _.each($scope.inquiries, function(inquiry){
            inquiry.selected = false;
          })
          $scope.inquiries = _.filter($scope.inquiries, function(inquiry){
            return inquiry.status == "PENDING";
          })
          growl.success("Inquiries Loaded", {ttl: 1000, disableCountDown: true});
        }
      })

      $scope.selectAll = false;
      $scope.selectAllI = false;
    }

    // Inquiries

    var rejectInquiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/rejectInquiryModal.html',
      show: false
    });

    $scope.showRejectInquiryModal = function(inquiryid){

      $scope.messagereject = ""
      $scope.inquirytoremove = inquiryid;
      rejectInquiryModal.$promise.then(rejectInquiryModal.show());

      if(inquiryid == 'selected'){
        $scope.selectedInquiries = $scope.getAllSelectedInquiries();
      } else {
        $scope.selectedInquiries = $scope.getInquiry(inquiryid)
      }
    }


    $scope.rejectInquiry = function(message){
      growl.info("Rejecting Inquiry", {ttl: 1000, disableCountDown: true});

      if($scope.inquirytoremove == 'selected'){ //bulk

        var selectedInquiries = $scope.selectedInquiries;

        _.each(selectedInquiries, function(inquiry){
          inquiry.r_message = message;
          inquiriesService.rejectInquiry(inquiry).then(function(resp){
          })
        });

        loadData();
        growl.success("Inquiries Rejected", {ttl: 1000, disableCountDown: true});

      } else {
        $scope.selectedInquiries[0].r_message = message;
        inquiriesService.rejectInquiry($scope.selectedInquiries).then(function(resp){

          loadData();
        })
      }
    }

    $scope.getInquiry = function(inquiryid){
      return _.filter($scope.inquiries, function(inquiry){ return inquiry._id == inquiryid })
    }

    $scope.hasSelectedI = function(){
      var selected = $scope.countSelectedInquiries()
      if(selected.selected > 0){ return true; }
      else { return false; }
    }

    $scope.selectAllInquiries = function(){
      $scope.inquiries = _.each($scope.inquiries, function(inquiry){
        inquiry.selected = $scope.selectAllI;
      })
    }

    $scope.countSelectedInquiries = function(){

      var selected = _.countBy($scope.inquiries, function(inquiry) {
        return inquiry.selected == true ? 'selected': 'not-selected';
      });

      return selected;
    }

    $scope.getAllSelectedInquiries = function(){
      return _.filter($scope.inquiries, function(inquiry){
        return inquiry.selected == true;
      })
    }

    var approveInquiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/approveInquiryModal.html',
      show: false
    });

    $scope.showApproveInquiryModal = function(inquiryid){

      $scope.messageapprove = ""
      $scope.inquirytoapprove = inquiryid;
      approveInquiryModal.$promise.then(approveInquiryModal.show());

      if(inquiryid == 'selected'){
        $scope.selectedInquiries = $scope.getAllSelectedInquiries();
      } else {
        $scope.selectedInquiries = $scope.getInquiry(inquiryid)
      }

    }

    $scope.approveInquiry = function(message){

      growl.info("Approving Inquiry & Creating User Account", {ttl: 1000, disableCountDown: true});

      var selectedInquiries = $scope.selectedInquiries;

      if($scope.inquirytoapprove == 'selected'){ //bulk

        _.each(selectedInquiries, function(inquiry){
          inquiry.r_message = message;
          inquiriesService.approveInquiry(inquiry._id).then(function(resp){
            if(inquiry._id == selectedInquiries[selectedInquiries.length - 1]._id){
              loadData();
              growl.success("Inquiries Rejected", {ttl: 1000, disableCountDown: true});
            }
          })
        });

      } else {
        inquiriesService.approveInquiry(selectedInquiries[0]._id).then(function(resp){
          inquiriesService.getInquiries().then(function(resp){
            if(resp.data.result != null) {
              $scope.inquiries = resp.data.result;
              growl.success("Inquiry Approved & User is created account", {ttl: 2000, disableCountDown: true});
              loadData();
            }
          })
        })
      }

    }


  });
