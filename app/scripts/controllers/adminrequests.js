'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminRequestsCtrl', function ($scope, User, $modal, requestService, growl) {
    User.loggedIn = true;
  	$scope.user = User;

    $scope.$on('$viewContentLoaded', function() {
      
      growl.info("Loading Requests", {ttl: 1000, disableCountDown: true});

      $scope.selectAll = false;

      requestService.getRequests().then(function(resp){
        if(resp.data.result != null) {
          $scope.requests = resp.data.result;
          growl.success("Requests Loaded", {ttl: 1000, disableCountDown: true});
        }
      })

    });

    var shareModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/shareRequest.html',
      show: false
    });

    $scope.showShareModal = function(requestid) {
      $scope.requestidtoshare = requestid;
      shareModal.$promise.then(shareModal.show());
      // $('#emails').tagsInput({'defaultText':'add email','height':'50px'});
    };

    $scope.shareRequest = function(requestid){
      var email = $('#emails').val();
      growl.info("Sending Request", {ttl: 1000, disableCountDown: true});
      requestService.shareRequest(requestid, email).then(function(resp){
        growl.success("Request Shared to " + email, {ttl: 3000, disableCountDown: true});
      })
    }

    var approveModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/approveModal.html',
      show: false
    });

    $scope.showApproveModal = function(requestid){
      $scope.requestidtoapprove = requestid;
      approveModal.$promise.then(approveModal.show());

      if(requestid == 'selected'){
        $scope.selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      } else {
        $scope.selectedPendingRequests = $scope.getRequest(requestid)
      }
    }

    $scope.approve = function(expirationdate){
      if(expirationdate == undefined) {
        growl.error("Please set an expiry date", {ttl: 1000, disableCountDown: true});
      } else {
        approveModal.hide()
        growl.info("Approving Request", {ttl: 1000, disableCountDown: true});
        console.log($scope.requestidtoapprove, expirationdate)

        if($scope.requestidtoapprove == 'selected'){ //bulk

          var selectedPendingRequests = $scope.getAllSelectedPendingRequest();

          _.each(selectedPendingRequests, function(request){
            requestService.approveRequest(request._id, expirationdate).then(function(resp){
            })
          });

          loadData();
          growl.success("Requests Approved", {ttl: 1000, disableCountDown: true});

        } else {
          requestService.approveRequest($scope.requestidtoapprove, expirationdate).then(function(resp){
            if(resp.data.result != null) {

              loadData();
            }
          })
        }
      }
    }

    var setExpiryModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/setExpiryModal.html',
      show: false
    });

    $scope.showSetExpiryModal = function(requestid){
      $scope.requestidtosetexpiry = requestid;
      setExpiryModal.$promise.then(setExpiryModal.show());
    }

    $scope.setExpiry = function(expirationdate){
      growl.info("Setting Expiry Date", {ttl: 1000, disableCountDown: true});

      var selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      requestService.setExpiryRequest($scope.requestidtosetexpiry, expirationdate).then(function(resp){
        if(resp.data.result != null) {

          loadData();
          growl.success("Expiry Date Set", {ttl: 1000, disableCountDown: true});
        }
      })

    }

    var disapproveRequestModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/modals/disapproveRequestModal.html',
      show: false
    });

    $scope.showDisapproveRequestModal = function(requestid){
      $scope.requestidtodisapprove = requestid;
      disapproveRequestModal.$promise.then(disapproveRequestModal.show());

      if(requestid == 'selected'){
        $scope.selectedPendingRequests = $scope.getAllSelectedPendingRequest();
      } else {
        $scope.selectedPendingRequests = $scope.getRequest(requestid)
      }
    }

    $scope.disapproveRequest = function(message){
      growl.info("Sending message", {ttl: 1000, disableCountDown: true});

      if($scope.requestidtodisapprove == 'selected'){ //bulk

        var selectedPendingRequests = $scope.getAllSelectedPendingRequest();

        _.each(selectedPendingRequests, function(request){
          requestService.disapproveRequest(request._id, message).then(function(resp){
          })
        });

        loadData();
        growl.success("Messages sent", {ttl: 1000, disableCountDown: true});

      } else {
        requestService.disapproveRequest($scope.requestidtodisapprove, message).then(function(resp){

          loadData();
        })
      }
    }

    $scope.getRequest = function(requestid){
      return _.filter($scope.requests, function(request){ return request._id == requestid })
    }

    $scope.hasSelected = function(){
      var selected = $scope.countSelectedPendingRequest()
      if(selected.selected > 0){ return true; }
      else { return false; }
    }

    $scope.selectAllPendingRequests = function(){
      $scope.requests = _.each($scope.requests, function(request){
        request.selected = $scope.selectAll;
      })
    }

    $scope.countSelectedPendingRequest = function(){

      var selected = _.countBy($scope.requests, function(request) {
        return request.selected == true ? 'selected': 'not-selected';
      });

      return selected;
    }

    $scope.getAllSelectedPendingRequest = function(){
      return _.filter($scope.requests, function(request){
        return request.selected == true;
      })
    }

    function loadData(){
      requestService.getRequests().then(function(resp){
        if(resp.data.result != null) {
          // $scope.pendingRequests = _.filter(resp.data.result, function(request){ return request.approve_tag == 'NO'; });

          $scope.requests = resp.data.result;
          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});
        }
      })
      $scope.selectAll = false;
    }


  });
