'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:AdminImportCtrl
 * @description
 * # AdminImportCtrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('AdminImportCtrl', function (ENV, $scope, User, $modal, _, $stateParams, testSetsService, growl) {
    User.loggedIn = true;
  	$scope.user = User;

    $scope.serverIP = ENV.apiEndpoint;

    $scope.$on('$viewContentLoaded', function() {
      growl.info("Loading Folders", {ttl: 1000, disableCountDown: true});

      testSetsService.getFolders().then(function(resp){
        $scope.folders = resp.data.result;
        growl.success("Folders Loaded", {ttl: 1000, disableCountDown: true});
      })
    });

    $scope.onUpload = function(data){
        growl.info("Uploading Image", {ttl: 1000, disableCountDown: true});
    }

    $scope.onComplete = function(resp){
      growl.success("Image Uploaded", {ttl: 1000, disableCountDown: true});
    }

    $scope.onUploadTestSet = function(data){
        growl.info("Uploading TestSets", {ttl: 1000, disableCountDown: true});
    }

    $scope.onCompleteTestSet = function(resp){
      growl.success("TestSets Uploaded", {ttl: 1000, disableCountDown: true});
    }

    $scope.onUploadScores = function(data){
        growl.info("Uploading Scores", {ttl: 1000, disableCountDown: true});
    }

    $scope.onCompleteScores = function(resp){
      growl.success("Scores Uploaded", {ttl: 1000, disableCountDown: true});
    }

    

    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
      renderDragger()
    });

    $scope.$watch('q', function() {
      renderDragger()
    });

    function renderDragger(){
      var holders = document.getElementsByClassName("folder");
      _.each(holders, function(holder){

        holder.ondragover = function () {
          if (this.classList)
            this.classList.add('hover');
          else
            this.className += ' ' + 'hover';
          return false;
        };
        holder.ondragend = function () {
          if (this.classList)
            this.classList.remove('hover');
          else
            this.className = this.className.replace(new RegExp('(^|\\b)' + 'hover'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
          return false;
        };
        holder.ondragleave = function () {
          if (this.classList)
            this.classList.remove('hover');
          else
            this.className = this.className.replace(new RegExp('(^|\\b)' + 'hover'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
          return false;
        };
        holder.ondrop = function (e) {
          this.className = this.className.replace(new RegExp('(^|\\b)' + 'hover'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
      })
    }
    

  });
