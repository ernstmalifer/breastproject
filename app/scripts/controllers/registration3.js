'use strict';

/**
 * @ngdoc function
 * @name breastApp.controller:Registration2Ctrl
 * @description
 * # Registration2Ctrl
 * Controller of the breastApp
 */
angular.module('breastApp')
  .controller('Registration3Ctrl', function ($rootScope, $scope, User, $location, requestService, _, growl) {

  	User.loggedIn = true;

  	$scope.$on('$viewContentLoaded', function() {
      window.scrollTo(0, 0);
	  });
    $scope.$on('$locationChangeStart', function (event, next, current) {
      requestidloaded(); //this will deregister that listener
    });

    var requestidloaded = $rootScope.$on('requestidloaded', function(event, mass) {

      growl.info("Loading Information", {ttl: 1000, disableCountDown: true});

      requestService.getRequest(User.requestid).then(function(resp){
        if(resp.data.result != null) {
          $scope.request = resp.data.result[0];
          $scope.request.redirectlink = window.location.hash;

          growl.success("Infomation Loaded", {ttl: 1000, disableCountDown: true});

          if($scope.request.message) {
            growl.error('<b>Admin Message:</b><br>' + $scope.request.message + '<br><em><small>Closing this message will remove this notification.</small></em>', {
              onclose: function() {
              },
              onopen: function() {
              }
            });
          }
          
          CKEDITOR.replace( 'status' );
          CKEDITOR.instances.status.on('blur', function(event){
            $scope.validateNoFundingStatus();
          })
        }
      })
    });

  	$scope.back = function(){
  		$location.path( '/registration/researchprojectinformation' );
  	};

    $scope.addFunder = function(){
      $scope.request.funding.two.push({})
    }

    $scope.removeFunder = function(index){
      $scope.request.funding.two.splice(index, 1) 
    }

  	$scope.save = function(){

      $scope.saving = true;

      if(validate()){
        // $scope.request.funding = {};
        $scope.request.funding.three = CKEDITOR.instances.status.getData()

        growl.info("Saving Infomation", {ttl: 1000, disableCountDown: true});

        requestService.saveRequest(User.requestid, $scope.request).then(function(resp){
          $scope.saving = false;
          growl.success("Infomation Saved", {ttl: 1000, disableCountDown: true});
        })
      } else {
        $scope.saving = false;
        growl.error("Please enter required fields before saving.", {ttl: 1000, disableCountDown: true});
      }

  	};

  	$scope.next = function(){
      if(validate()){
        $scope.save();
  		  $location.path( '/registration/resourcesrequired' );
      } else {
        growl.error("Please enter required fields and save before continue.", {ttl: 1000, disableCountDown: true});
      }
  	};

    function validate(){

      $scope.validateSecuredFundingResearchProject()
      $scope.validateFundingBody()
      $scope.validateFundingYears ()
      $scope.validateFundingAUD()
      $scope.validateNoFundingStatus()
      $scope.validateEthics()

      if( !$scope.fundingone_error &&
        !$scope.status_error &&
        !$scope.fundingtwofundingbody_error &&
        !$scope.fundingtwoyears_error &&
        !$scope.fundingtwoaud_error &&
        !$scope.ethicshrec_error
      ){
        return true;
      } else {
        return false;
      }

    }

    $scope.validateSecuredFundingResearchProject = function(){

      $scope.fundingone_error = false

      if(validator.isNull($scope.request.funding)){
        $scope.fundingone_error = true;
        growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
      } else {
      }
    }

    $scope.validateFundingBody = function(){

      $scope.fundingone_error = false
      $scope.fundingtwofundingbody_error = false
      
      if(validator.isNull($scope.request.funding)){
        $scope.fundingone_error = true;
        growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.funding.one)){
          $scope.fundingone_error = true;
          growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
        } else {
          if($scope.request.funding.one == 'No'){
            
          } else {
            // if(validator.isNull($scope.request.funding.two)){
            //   $scope.fundingtwofundingbody_error = true;
            //   growl.error("Please enter Funding Body", {ttl: 1000, disableCountDown: true});
            // } else {
            //   if(validator.isNull($scope.request.funding.two.fundingbody)){
            //     $scope.fundingtwofundingbody_error = true;
            //     growl.error("Please enter Funding Body", {ttl: 1000, disableCountDown: true});
            //   } else {
            //   }
            // }
          }
        }
      }
    }

    $scope.validateFundingYears = function(){
      
      $scope.fundingone_error = false
      $scope.fundingtwoyears_error = false
      
      if(validator.isNull($scope.request.funding)){
        $scope.fundingone_error = true;
        growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.funding.one)){
          $scope.fundingone_error = true;
          growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
        } else {
          if($scope.request.funding.one == 'No'){
            
          } else {
            // if(validator.isNull($scope.request.funding.two)){
            //   $scope.fundingtwoyears_error = true;
            //   growl.error("Please enter length or approved funding period in years", {ttl: 1000, disableCountDown: true});
            // } else {
            //   if(validator.isNull($scope.request.funding.two.years)){
            //     $scope.fundingtwoyears_error = true;
            //     growl.error("Please enter length or approved funding period in years", {ttl: 1000, disableCountDown: true});
            //   } else {
            //     if(!validator.isInt($scope.request.funding.two.years)){
            //       $scope.fundingtwoyears_error = true;
            //       growl.error("Please enter length or approved funding period in years", {ttl: 1000, disableCountDown: true});
            //     }
            //   }
            // }
          }
        }
      }
    }

    $scope.validateFundingAUD = function(){
      
      $scope.fundingone_error = false
      $scope.fundingtwoaud_error = false
      
      if(validator.isNull($scope.request.funding)){
        $scope.fundingone_error = true;
        growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.funding.one)){
          $scope.fundingone_error = true;
          growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
        } else {
          if($scope.request.funding.one == 'No'){
            
          } else {
            // if(validator.isNull($scope.request.funding.two)){
            //   $scope.fundingtwoaud_error = true;
            //   growl.error("Please enter Amount in AUD", {ttl: 1000, disableCountDown: true});
            // } else {
            //   if(validator.isNull($scope.request.funding.two.aud)){
            //     $scope.fundingtwoaud_error = true;
            //     growl.error("Please enter Amount in AUD", {ttl: 1000, disableCountDown: true});
            //   } else {
            //     if(!validator.isInt($scope.request.funding.two.aud)){
            //       $scope.fundingtwoaud_error = true;
            //       growl.error("Please enter Amount in AUD", {ttl: 1000, disableCountDown: true});
            //     }
            //   }
            // }
          }
        }
      }
    }

    $scope.validateNoFundingStatus = function(){
      
      $scope.fundingone_error = false
      $scope.status_error = false

      if(validator.isNull($scope.request.funding)){
        $scope.fundingone_error = true;
        growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.funding.one)){
          $scope.fundingone_error = true;
          growl.error("Please select if you have secured funding for the research project", {ttl: 1000, disableCountDown: true});
        } else {
          if($scope.request.funding.one == 'No'){
            if(validator.isNull(CKEDITOR.instances.status.getData())){
              $scope.status_error = true;
              growl.error("Please enter the status of your project", {ttl: 1000, disableCountDown: true});
            } else {
            }
          }
        }
      }

    }

    $scope.validateEthics = function(){

      $scope.ethicshrec_error = false

      if(validator.isNull($scope.request.ethics)){
        $scope.ethicshrec_error = true;
        growl.error("Please select if you have Human Research Ethics Committee (HREC) approval to conduct this research project", {ttl: 1000, disableCountDown: true});
      } else {
        if(validator.isNull($scope.request.ethics.hrec)){
          $scope.ethicshrec_error = true;
          growl.error("Please select if you have Human Research Ethics Committee (HREC) approval to conduct this research project", {ttl: 1000, disableCountDown: true});
        }
      }
    }

  });

