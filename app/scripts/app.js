'use strict';

/**
 * @ngdoc overview
 * @name breastApp
 * @description
 * # breastApp
 *
 * Main module of the application.
 */
angular
  .module('breastApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'restangular',
    'mgcrea.ngStrap',
    'angular-growl',
    'lr.upload',
    'simplePagination'
  ])
  .config(function ($stateProvider, $urlRouterProvider, RestangularProvider, $httpProvider) {
  // .config(function ($stateProvider, $urlRouterProvider) {

    $httpProvider.defaults.withCredentials = true;

    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('main', {
      url: '/?login',
      views: {
        'mainView': {
          templateUrl: 'views/main.html',
          controller: 'MainCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('pages', {
      url: '/pages/:permalink',
      views: {
        'mainView': {
          templateUrl: 'views/page.html',
          controller: 'PageCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('login', {
      url: '/login?login',
      views: {
        'mainView': {
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('verify', {
      url: '/verify/:id',
      views: {
        'mainView': {
          templateUrl: 'views/verify.html',
          controller: 'VerifyCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('404', {
      url: '/404',
      views: {
        'mainView': {
          templateUrl: 'views/404.html',
          controller: '404Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('projecttitle', {
      url: '/registration/projecttitle',
      views: {
        'mainView': {
          templateUrl: 'views/registration1.html',
          controller: 'Registration1Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('researchprojectinformation', {
      url: '/registration/researchprojectinformation',
      views: {
        'mainView': {
          templateUrl: 'views/registration2.html',
          controller: 'Registration2Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('funding', {
      url: '/registration/funding',
      views: {
        'mainView': {
          templateUrl: 'views/registration3.html',
          controller: 'Registration3Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('resourcesrequired', {
      url: '/registration/resourcesrequired',
      views: {
        'mainView': {
          templateUrl: 'views/registration4.html',
          controller: 'Registration4Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('certificationbyapplicant', {
      url: '/registration/certificationbyapplicant',
      views: {
        'mainView': {
          templateUrl: 'views/registration5.html',
          controller: 'Registration5Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('applicantchecklist', {
      url: '/registration/applicantchecklist',
      views: {
        'mainView': {
          templateUrl: 'views/registration6.html',
          controller: 'Registration6Ctrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Researcher']
      }
    })
    .state('search', {
      url: '/search',
      views: {
        'mainView': {
          templateUrl: 'views/search.html',
          controller: 'SearchCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator', 'Researcher']
      }
    })
    .state('userprofile', {
      url: '/userprofile',
      views: {
        'mainView': {
          templateUrl: 'views/userprofile.html',
          controller: 'UserProfileCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator', 'Researcher']
      }
    })
    .state('admin', {
      url: '/admin',
      views: {
        'mainView': {
          templateUrl: 'views/admin/admin.html',
          controller: 'AdminCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('adminuserlist', {
      url: '/admin/users',
      views: {
        'mainView': {
          templateUrl: 'views/admin/users.html',
          controller: 'AdminUsersCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('adminuser', {
      url: '/admin/users/:id',
      views: {
        'mainView': {
          templateUrl: 'views/admin/user.html',
          controller: 'AdminUserCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('adminrequestlist', {
      url: '/admin/requests',
      views: {
        'mainView': {
          templateUrl: 'views/admin/requests.html',
          controller: 'AdminRequestsCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('adminrequestitem', {
      url: '/admin/requests/:id',
      views: {
        'mainView': {
          templateUrl: 'views/admin/request.html',
          controller: 'AdminRequestCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('admininquirieslist', {
      url: '/admin/inquiries',
      views: {
        'mainView': {
          templateUrl: 'views/admin/inquiries.html',
          controller: 'AdminInquiriesCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('admininquiryitem', {
      url: '/admin/inquiries/:id',
      views: {
        'mainView': {
          templateUrl: 'views/admin/inquiry.html',
          controller: 'AdminInquiryCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    })
    .state('admininimport', {
      url: '/admin/import',
      views: {
        'mainView': {
          templateUrl: 'views/admin/import.html',
          controller: 'AdminImportCtrl'
        },
        'userHeaderView': {
          templateUrl: 'views/user.html',
          controller: 'UserCtrl'
        }
      },
      access: {
        requireLogin: true,
        requiredPermissions: ['Administrator']
      }
    });

    RestangularProvider.setDefaultHttpFields({'withCredentials': true});

  })
  // .run(function ($rootScope, $location, $window, $q, $http){
  .run(function ($rootScope, $location, Restangular, $window, userService, requestService, User, _, ENV, $interval, growl){

    var heartbeat;

  	Restangular.setBaseUrl(ENV.apiEndpoint + '/api/1.0');

    // set params for multiple methods at once
    Restangular.setFullResponse(true);

    Restangular.setErrorInterceptor(
          function(response) {
              if (response.status == 401) {
                  $window.location.href='#/login?login=false';
              } else if (response.status == 404) {
              } else if (response.status == 500) {
                if(response.data.msg.msg == "Email/Username already exist") {
                  growl.error(response.data.msg.msg, {ttl: 2000, disableCountDown: true});
                } else {
                  growl.error(response.data.msg.msg, {ttl: 2000, disableCountDown: true});
                  $window.location.href='#/';
                }
              } else {
                  growl.error("Could not connect to server: " + response.status, {ttl: 1000, disableCountDown: true})
                  console.log("Response received with HTTP error code: ERROR" + response.status );
              }
              return true; // false to stop the promise chain
          }
      );

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams){

      $interval.cancel(heartbeat);

      userService.islogin().then(function(resp){
        if (resp.data == '0') {
          if(toState.access){
            $window.location.href='#/404';
          }
        }
        else {

          heartbeat = $interval(checkLogin, 5000);

          // Get User Data
          User.loggedIn = true;
          User._id = resp.data._id
          User.u_email = resp.data.email
          User.role = resp.data.roleID.RoleType

          // Get User Profile
          userService.getUser(User._id).then(function(resp){

            User.u_fname= resp.data.result.u_fname
            User.u_mi = resp.data.result.u_mi
            User.u_lname= resp.data.result.u_lname
            User.department= resp.data.result.department
            User.institution= resp.data.result.institution
            User.postaladdress= resp.data.result.postal_address
            User.city_suburb= resp.data.result.city_suburb
            User.phone= resp.data.result.phone
            User.fax= resp.data.result.fax
            User.mobile= resp.data.result.mobile
            User.avatar= resp.data.result.u_image

            $rootScope.$emit('userprofileloaded');
          });

          // Check Access
          if(toState.access){
            if(toState.access.requireLogin) {
              //check Privileges
              if(toState.access.requiredPermissions){

                // If Researcher
                if(_.indexOf(toState.access.requiredPermissions, 'Researcher') != -1){
                  requestService.getUnapprovedRequestByUser(User._id).then(function(resp){

                    if (resp.data.result != null){
                      if(resp.data.result.approve_tag == 'NO'){
                        User.requestid = resp.data.result._id
                        $rootScope.$emit('requestidloaded');
                      } else {
                      }
                    } else {
                      $rootScope.$emit('requestidempty');
                    }
                  });
                }

                // If Administrator
                else if(_.indexOf(toState.access.requiredPermissions, 'Administrator') != -1){
                  requestService.getUnapprovedRequestByUser(User._id).then(function(resp){

                    if (resp.data.result != null){
                      if(resp.data.result.approve_tag == 'NO'){
                        User.requestid = resp.data.result._id
                        $rootScope.$emit('requestidloaded');
                      } else {
                      }
                    } else {
                      $rootScope.$emit('requestidempty');
                    }
                  });
                }

                if(_.indexOf(toState.access.requiredPermissions, User.role) != -1){
                } else {
                  $window.location.href='#/404';
                }
              }
            }
          }
        }
      });

    });

    function checkLogin(){
      // userService.islogin().then(function(resp){
      //   if (resp.data == '0') {
      //     growl.danger('Your session has been expired, please login again.')
      //   }
      //   else {
      //     console.log(new Date)
      //   }
      // });
    }

  });

angular.module('breastApp').value( 'User', {
  'loggedIn': false,
  '_id': '',
  'f_name': '',
  'l_name': '',
  'institution': '',
  'postal_address': '',
  'city_suburb': '',
  'phone': '',
  'fax': '',
  'mobile': '',
  'email': '',
  'avatar': '',
  'requestid': '',
  'role': ''
});