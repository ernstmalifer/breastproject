'use strict';

/**
 * @ngdoc service
 * @name breastApp.requestService
 * @description
 * # requestService
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('requestService', function (Restangular, _) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getRequests = function(tag){
      if(tag){
        if(tag == 'yes'){ return Restangular.all('request_status').customGET('YES') }
        else if(tag == 'no'){ return Restangular.all('request_status').customGET('NO') }
      } else {
        return Restangular.all('request').customGET();
      }
    };

    this.getUnapprovedRequestByUser = function(userid){
      return Restangular.all('request').customGET(userid + '/NO')
      // 54fe4101c0d9f694162f8607/NO
    }

    this.getRequest = function(requestid){
      return Restangular.all('requestSP').customGET(requestid)
    };

    this.saveRequest = function(requestid, request){
      // request = _.omit(request, '_id');
      // request = _.omit(request, 'userID');
      // request = _.omit(request, 'userprofileID');

      // console.log(request)
      return Restangular.all('requestSP').customPUT(request, requestid, {}, {})
    };

    this.approveRequest = function(requestid, expirationDate){
      return Restangular.all('approveRequest').customPUT({expirationDate: expirationDate}, requestid, {}, {})
    };

    this.setExpiryRequest = function(requestid, expirationDate){
      return Restangular.all('requestSP').customPUT({expirationDate: expirationDate}, requestid, {}, {})
    };

    this.revokeRequest = function(requestid){
      return Restangular.all('requestSP').customPUT({approve_tag: "NO"}, requestid, {}, {})
    };

    this.shareRequest = function(requestid, emails){
      // console.log(emails)
      emails = emails.split(",")
      emails = _.each(emails, function(email){ email = email.trim() } );
      // console.log(emails)
      var share = {requestID: [requestid], email: emails};
      return Restangular.all('shareApplication').customPOST(share, '', {}, {})
    }

    this.disapproveRequest = function(requestid, message){
      return Restangular.all('disapproveRequest').customPUT({message: message}, requestid);
    }

    this.submitRequest = function(requestid){
      return Restangular.all('sendEmailRequestToAdmin').customGET(requestid);
    }

    this.deleteRequest = function(requestid){
      return Restangular.all('requestSP').customDELETE(requestid)
    }
  
  });
