'use strict';

/**
 * @ngdoc service
 * @name breastApp.uploadService
 * @description
 * # uploadService from http://52.11.64.116:3000/api
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('uploadService', function (Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getSuburbs = function(q){
        return Restangular.all('accessAPI').customPOST({searchQ: '?q=' + q})
    };

  });