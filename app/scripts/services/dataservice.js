'use strict';

/**
 * @ngdoc service
 * @name breastApp.dataService
 * @description
 * # dataService
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('dataService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getAllData = function(){

      var sampleData = [
        {
          id: 1,
          title: 'Data 1',
          testSet: 1, //Hobart
          name: 'Ernst Malifer',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse aspernatur, maxime, nemo quo similique eveniet molestiae officiis dolor minus. Molestias.',
          dataType: { 
            value: 1, //Readings
            readerType: 1, //Radiologist
            dataType: 1, //Scores
          }
        },
        {
          id: 2,
          title: 'Data 2',
          testSet: 1, //Hobart
          name: 'Jean Inoc',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, necessitatibus, vel! Recusandae consectetur animi earum dignissimos neque quaerat eum vitae.',
          dataType: { 
            value: 2, //Images
            caseType: 1, //Normal
            normalType: 4, //Extremely Dense
          }
        }
      ]

      return sampleData;

    }

  });

'use strict';