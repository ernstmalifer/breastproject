'use strict';

/**
 * @ngdoc service
 * @name breastApp.testSetsService
 * @description
 * # testSetsService
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('testSetsService', function (Restangular, _) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getTestSetsColumns = function(){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('testsetsColumns').customGET('');
      
    };

    this.getTestSetsColumnValue = function(columnname){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('testsets/' + columnname).customGET('');
      
    };

    this.getScoresColumns = function(){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('scoreColumns').customGET('');
      
    };

    this.getScoresColumnValue = function(columnname){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('scores/' + columnname).customGET('');
      
    };

    this.getTestsets = function(){
      return Restangular.all('testsets').customGET('');
    }

    this.download = function(userID, testSetsIDS, downloadType, dataType, email, selectedColumns, imagetype){

      // selectedColumns.push('DICOM_folder_ID')
      // console.log(selectedColumns)

      selectedColumns = _.without(selectedColumns, 'Thumbnail')

      switch(downloadType){
        case 'zip':
          return Restangular.all('download').withHttpConfig().customPOST(
          {
            userID: userID,
            testsetsID: testSetsIDS,
            downloadType: downloadType,
            dataType: dataType,
            email: email,
            imageType: imagetype,
            columns: selectedColumns,
            timestamp: Date.now()
          }, '');
          break;
        case 'email':
        case 'googledrive':
        case 'dropbox':
          return Restangular.all('download').customPOST(
          {
            userID: userID,
            testsetsID: testSetsIDS,
            downloadType: downloadType,
            dataType: dataType,
            email: email,
            imageType: imagetype,
            columns: selectedColumns,
            timestamp: Date.now()
          }, '');
          break;
        default:
          break;
      }

    }

    this.getFolders = function(){
      return Restangular.all('folderList').customGET();
    }

  });