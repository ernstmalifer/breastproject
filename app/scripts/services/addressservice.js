'use strict';

/**
 * @ngdoc service
 * @name breastApp.addressService
 * @description
 * # addressService from http://52.11.64.116:3000/api
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('addressService', function (Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getSuburbs = function(q){
        return Restangular.all('accessAPI').customPOST({searchQ: '?q=' + q})
    };

    this.getCities = function(){
      return Restangular.all('city').customGET();
    }


  });