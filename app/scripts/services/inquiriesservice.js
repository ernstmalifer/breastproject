'use strict';

/**
 * @ngdoc service
 * @name breastApp.testSetsService
 * @description
 * # testSetsService
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('inquiriesService', function (Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getInquiries = function(){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('inquiry').customGET('');
      
    };

    this.saveInquiry = function(inquiry){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('inquiry').customPOST(inquiry);
      
    };

    this.approveInquiry = function(inquiry){

      // if(_.isArray(inquiry)){
      //   inquiry = inquiry[0];
      // }

      // console.log(inquiry)
      // inquiry.status = 'APPROVED'
      // console.log(inquiry)

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('inquiry').customPOST({status: 'APPROVED', r_message: 'Your inquiry has been approved'},inquiry);
      
    };

    this.rejectInquiry = function(inquiry){

      if(_.isArray(inquiry)){
        inquiry = inquiry[0];
      }

      inquiry.status = 'REJECTED'

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('inquiry').customPOST(inquiry,inquiry._id);
      
    };


    this.removeInquiry = function(inquiryid){

      //https://github.com/mgonto/restangular#custom-methods
      return Restangular.all('inquiry').customDELETE(inquiryid);
      
    };


  });
