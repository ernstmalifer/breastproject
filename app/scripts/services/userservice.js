'use strict';

/**
 * @ngdoc service
 * @name breastApp.userService
 * @description
 * # userService from http://52.11.64.116:3000/api
 * Service in the breastApp.
 */
angular.module('breastApp')
  .service('userService', function ($q, $http, $location, Restangular) {
  // .service('userService', function ($q, $http) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.email = '';

    this.login = function(username, password){

      var login = {username: username, password: password};

      return Restangular.all('login').post(login);
      // return $http.post('http://localhost:3000/api/1.0/login', login)
    };

    this.islogin = function(){
      return Restangular.all('login').customGET();
      // return $http.get('http://localhost:3000/api/1.0/login')
    };

    this.logout = function(){
      return Restangular.all('logout').customGET();
    };

    this.getAllUsers = function(){
      return Restangular.all('users').customGET();
    };

    this.getUser = function(userid){
      return Restangular.all('user').customGET(userid);
    };

    this.saveUser = function(userid, user){
      return Restangular.all('user').customPUT(user, userid);
    };

    this.updateUser = function(userid, user){
      return Restangular.all('user').customPUT(user, userid)
    };

    this.uploadUserImage = function(userid, image){

    };

    this.verify = function(userid, password, confirm){
      return Restangular.all('user').customPOST({password: password, confirm: confirm}, 'verify/' + userid);
    }

    this.recoverPassword = function(email){
      return Restangular.all('email').customPOST({u_email: email}, 'recoverPassword');
    }

    this.createUser = function(user){
      return Restangular.all('users').customPOST(user);
    };

    this.deleteUser = function(userid){
      console.log(userid)
      return Restangular.all('user').customDELETE(userid)
    }

    this.getRoles = function(){
      return Restangular.all('roles').customGET()
    }

    this.getNoOfUsersLogin = function(){
      return Restangular.all('NoofUserLogin').customGET()
    }


  });

'use strict';