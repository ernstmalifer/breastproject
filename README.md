# BREAST

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Requirements

node & npm, grunt, bower, ruby for sass

`npm install`
`bower install`

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
