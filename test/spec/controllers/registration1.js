'use strict';

describe('Controller: Registration1Ctrl', function () {

  // load the controller's module
  beforeEach(module('breastApp'));

  var Registration1Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Registration1Ctrl = $controller('Registration1Ctrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
