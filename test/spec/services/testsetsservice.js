'use strict';

describe('Service: testSetsService', function () {

  // load the service's module
  beforeEach(module('breastApp'));

  // instantiate service
  var testSetsService;
  beforeEach(inject(function (_testSetsService_) {
    testSetsService = _testSetsService_;
  }));

  it('should do something', function () {
    expect(!!testSetsService).toBe(true);
  });

});
